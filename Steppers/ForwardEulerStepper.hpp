//
// Created by Nikita Kruk on 12.06.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_STEPPER_HPP
#define SPC2FVMNONLINNONLOCEQS_STEPPER_HPP

#include "../Definitions.hpp"
#include "../DynamicalSystems/DynSysFvmNne.hpp"
#include "../Parallelization/Thread.hpp"

#include <vector>
#include <iostream>
#include <algorithm> // std::fill

/**
 * Stepper implements the evolution of a quantity over time,
 * as is dictated by the RHS of a dynamical system
 */
class ForwardEulerStepper
{
 public:

  template<class T>
  void DoStep(Thread &thread, T &system, std::vector<Real> &system_state) const
  {
    static std::vector<Real> flux_x(system_state.size(), (Real) 0.0);
    std::fill(flux_x.begin(), flux_x.end(), 0.0);
    static std::vector<Real> flux_y(system_state.size(), (Real) 0.0);
    std::fill(flux_y.begin(), flux_y.end(), 0.0);
    static std::vector<Real> flux_phi(system_state.size(), (Real) 0.0);
    std::fill(flux_phi.begin(), flux_phi.end(), 0.0);

    system.CalculateFluxes(system_state, flux_x, flux_y, flux_phi);
    thread.SynchronizeVector(flux_x);
    thread.SynchronizeVector(flux_y);
    thread.SynchronizeVector(flux_phi);

    // Update cell-averaged densities
    int i = 0, j = 0, k = 0;
    int idx_prev_x = 0, idx_prev_y = 0, idx_prev_phi = 0;
    for (int idx : thread.GetLoopIndices())
    {
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      utilities::ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev_x);
      utilities::ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev_y);
      utilities::ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev_phi);
      system_state[idx] +=
          (-kDt / kDx * (flux_x[idx] - flux_x[idx_prev_x])
              - kDt / kDy * (flux_y[idx] - flux_y[idx_prev_y])
              - kDt / kDphi * (flux_phi[idx] - flux_phi[idx_prev_phi]));
    } // idx

    thread.SynchronizeVector(system_state);
  }
};

#endif //SPC2FVMNONLINNONLOCEQS_STEPPER_HPP
