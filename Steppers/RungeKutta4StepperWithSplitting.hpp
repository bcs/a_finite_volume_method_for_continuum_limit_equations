//
// Created by Nikita Kruk on 27.11.18.
//

#ifndef SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPER_HPP
#define SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPER_HPP

#include "../Definitions.hpp"
#include "../DynamicalSystems/DynSysSplitting.hpp"
#include "../Parallelization/Thread.hpp"
#include "../DynamicalSystems/VortexArraysSystemSplitting.hpp"

#include <vector>

class RungeKutta4StepperWithSplitting
{
 public:

  explicit RungeKutta4StepperWithSplitting(Thread *thread, Real dt);
  ~RungeKutta4StepperWithSplitting();

  Real GetDt() const
  { return dt_; }
  const std::vector<int> &GetAverageFluxLimiterCount() const
  { return average_flux_limiter_count_; }

  void DoStep(DynSysSplitting &system, std::vector<Real> &system_state);
  Real DoStepInPlace(DynSysSplitting &system,
                     std::vector<Real> &system_state,
                     Real dt);
  void DoStep(VortexArraysSystemSplitting &system, std::vector<Real> &system_state);

 private:

  Thread *thread_;
  Real dt_;
  int order_;
  std::vector<Real> k_1_;
  std::vector<Real> k_2_;
  std::vector<Real> k_3_;
  std::vector<Real> k_4_;
  std::vector<int> average_flux_limiter_count_;

  Real DoAdvectionStep(DynSysSplitting &system,
                       std::vector<Real> &system_state,
                       Real dt);
  Real DoSynchronizationStep(DynSysSplitting &system,
                             std::vector<Real> &system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const DynSysSplitting &system);

  Real DoAdvectionStep(VortexArraysSystemSplitting &system,
                       std::vector<Real> &system_state_,
                       Real dt);
  Real DoSynchronizationStep(VortexArraysSystemSplitting &system,
                             std::vector<Real> &system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const VortexArraysSystemSplitting &system);

};

#endif //SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPER_HPP
