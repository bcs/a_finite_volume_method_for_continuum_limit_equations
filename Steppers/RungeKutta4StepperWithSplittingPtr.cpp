//
// Created by Nikita Kruk on 2019-01-03.
//

#include "RungeKutta4StepperWithSplittingPtr.hpp"

#include <mpi.h>
#include <iostream>
#include <algorithm> // std::min_element

RungeKutta4StepperWithSplittingPtr::RungeKutta4StepperWithSplittingPtr(ThreadXySharedMemory *thread,
                                                                       Real dt) :
    thread_(thread),
    dt_(dt),
    order_(4),
    k_1_((unsigned long) (kN * kM * kL), 0.0),
    k_2_((unsigned long) (kN * kM * kL), 0.0),
    k_3_((unsigned long) (kN * kM * kL), 0.0),
    k_4_((unsigned long) (kN * kM * kL), 0.0),
    average_flux_limiter_count_(kDim, 0)
{

}

RungeKutta4StepperWithSplittingPtr::~RungeKutta4StepperWithSplittingPtr()
{
  k_1_.clear();
  k_2_.clear();
  k_3_.clear();
  k_4_.clear();
}

void RungeKutta4StepperWithSplittingPtr::DoStep(DynSysSplittingPtr &system, Real *const system_state)
{
  system.ResetFluxLimiterCount();

  Real new_dts[3] = {0.0};
  new_dts[0] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[0] *= 2.0;

  new_dts[1] = DoSynchronizationStep(system, system_state, dt_);

  new_dts[2] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[2] *= 2.0;

  FindAveragedFluxLimiterCounts(system);
//  dt_ = *std::min_element(new_dts, new_dts + 3);
//  thread_->FindMinValue(dt_);
}

Real RungeKutta4StepperWithSplittingPtr::DoAdvectionStep(DynSysSplittingPtr &system,
                                                         Real *const system_state,
                                                         Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);

  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateAdvection(system_state, k_1_, k_1_, 0.0, dt);
  new_dts[1] = system.CalculateAdvection(system_state, k_1_, k_2_, 0.5, dt);
  new_dts[2] = system.CalculateAdvection(system_state, k_2_, k_3_, 0.5, dt);
  new_dts[3] = system.CalculateAdvection(system_state, k_3_, k_4_, 1.0, dt);

  /*MPI_Win &win = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win);*/
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    system_state[idx] += (k_1_[idx] + 2.0 * k_2_[idx] + 2.0 * k_3_[idx] + k_4_[idx]) * dt / 6.0;
  } // idx
  /*MPI_Win_sync(win);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win);
  thread_->SynchronizeVectorThroughoutClusters(system_state);*/

  return *std::min_element(new_dts.begin(), new_dts.end());
}

Real RungeKutta4StepperWithSplittingPtr::DoSynchronizationStep(DynSysSplittingPtr &system,
                                                               Real *const system_state,
                                                               Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);

  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateSynchronization(system_state, k_1_, k_1_, 0.0, dt);
  new_dts[1] = system.CalculateSynchronization(system_state, k_1_, k_2_, 0.5, dt);
  new_dts[2] = system.CalculateSynchronization(system_state, k_2_, k_3_, 0.5, dt);
  new_dts[3] = system.CalculateSynchronization(system_state, k_3_, k_4_, 1.0, dt);

  /*MPI_Win &win = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win);*/
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    system_state[idx] += (k_1_[idx] + 2.0 * k_2_[idx] + 2.0 * k_3_[idx] + k_4_[idx]) * dt / 6.0;
  } // idx
  /*MPI_Win_sync(win);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win);
  thread_->SynchronizeVectorThroughoutClusters(system_state);*/

  return *std::min_element(new_dts.begin(), new_dts.end());
}

void RungeKutta4StepperWithSplittingPtr::FindAveragedFluxLimiterCounts(const DynSysSplittingPtr &system)
{
  average_flux_limiter_count_ = system.GetFluxLimiterCount();
//  if (thread_->IsRoot())
//  {
//    std::cout << "counts: " << average_flux_limiter_count_[0] << " " << average_flux_limiter_count_[1] << " "
//              << average_flux_limiter_count_[2] << std::endl;
//  }
  thread_->ComputeSumIntoRootOnly(average_flux_limiter_count_);
  average_flux_limiter_count_[0] = average_flux_limiter_count_[0] / (2 * order_); // 2 for two advection steps
  average_flux_limiter_count_[1] = average_flux_limiter_count_[1] / (2 * order_);
  average_flux_limiter_count_[2] = average_flux_limiter_count_[2] / (order_);
}

void RungeKutta4StepperWithSplittingPtr::DoStep(VortexArraysSystemSplittingBasicPtr &system,
                                                Real *const system_state)
{
  system.ResetFluxLimiterCount();

  Real new_dts[3] = {0.0};
  new_dts[0] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[0] *= 2.0;

  new_dts[1] = DoSynchronizationStep(system, system_state, dt_);

  new_dts[2] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[2] *= 2.0;

  FindAveragedFluxLimiterCounts(system);
//  dt_ = *std::min_element(new_dts, new_dts + 3);
//  thread_->FindMinValue(dt_);
}

Real RungeKutta4StepperWithSplittingPtr::DoAdvectionStep(VortexArraysSystemSplittingBasicPtr &system,
                                                         Real *const system_state,
                                                         Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);

  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateAdvection(system_state, k_1_, k_1_, 0.0, dt);
  new_dts[1] = system.CalculateAdvection(system_state, k_1_, k_2_, 0.5, dt);
  new_dts[2] = system.CalculateAdvection(system_state, k_2_, k_3_, 0.5, dt);
  new_dts[3] = system.CalculateAdvection(system_state, k_3_, k_4_, 1.0, dt);

  /*MPI_Win &win = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win);*/
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    system_state[idx] += (k_1_[idx] + 2.0 * k_2_[idx] + 2.0 * k_3_[idx] + k_4_[idx]) * dt / 6.0;
  } // idx
  /*MPI_Win_sync(win);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win);
  thread_->SynchronizeVectorThroughoutClusters(system_state);*/

  return *std::min_element(new_dts.begin(), new_dts.end());
}

Real RungeKutta4StepperWithSplittingPtr::DoSynchronizationStep(VortexArraysSystemSplittingBasicPtr &system,
                                                               Real *const system_state,
                                                               Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);

  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateSynchronization(system_state, k_1_, k_1_, 0.0, dt);
  new_dts[1] = system.CalculateSynchronization(system_state, k_1_, k_2_, 0.5, dt);
  new_dts[2] = system.CalculateSynchronization(system_state, k_2_, k_3_, 0.5, dt);
  new_dts[3] = system.CalculateSynchronization(system_state, k_3_, k_4_, 1.0, dt);

  /*MPI_Win &win = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win);*/
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    system_state[idx] += (k_1_[idx] + 2.0 * k_2_[idx] + 2.0 * k_3_[idx] + k_4_[idx]) * dt / 6.0;
  } // idx
  /*MPI_Win_sync(win);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win);
  thread_->SynchronizeVectorThroughoutClusters(system_state);*/

  return *std::min_element(new_dts.begin(), new_dts.end());
}

void RungeKutta4StepperWithSplittingPtr::FindAveragedFluxLimiterCounts(const VortexArraysSystemSplittingBasicPtr &system)
{
  average_flux_limiter_count_ = system.GetFluxLimiterCount();
//  if (thread_->IsRoot())
//  {
//    std::cout << "counts: " << average_flux_limiter_count_[0] << " " << average_flux_limiter_count_[1] << " "
//              << average_flux_limiter_count_[2] << std::endl;
//  }
  thread_->ComputeSumIntoRootOnly(average_flux_limiter_count_);
  average_flux_limiter_count_[0] = average_flux_limiter_count_[0] / (2 * order_); // 2 for two advection steps
  average_flux_limiter_count_[1] = average_flux_limiter_count_[1] / (2 * order_);
  average_flux_limiter_count_[2] = average_flux_limiter_count_[2] / (order_);
}