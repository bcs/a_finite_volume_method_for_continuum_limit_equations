//
// Created by Nikita Kruk on 2018-12-06.
//

#include "RungeKutta3StepperWithSplitting.hpp"
#include "../DynamicalSystems/DynSysSplittingStructPresFvm.hpp"

#include <algorithm> // std::fill, std::transform
#include <iostream>
#include <functional> // std::plus

RungeKutta3StepperWithSplitting::RungeKutta3StepperWithSplitting(Thread *thread, Real dt) :
    thread_(thread),
    dt_(dt),
    order_(3),
    k_1_((unsigned long) (kN * kM * kL), 0.0),
    k_2_((unsigned long) (kN * kM * kL), 0.0),
    k_3_((unsigned long) (kN * kM * kL), 0.0),
    average_flux_limiter_count_(kDim, 0)
{

}

RungeKutta3StepperWithSplitting::~RungeKutta3StepperWithSplitting()
{
  k_1_.clear();
  k_2_.clear();
  k_3_.clear();
}

void RungeKutta3StepperWithSplitting::DoStep(DynSysSplitting &system,
                                             std::vector<Real> &system_state)
{
  system.ResetFluxLimiterCount();

  Real new_dts[3] = {0.0};
  new_dts[0] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[0] *= 2.0;

  new_dts[1] = DoSynchronizationStep(system, system_state, dt_);

  new_dts[2] = DoAdvectionStep(system, system_state, dt_ / 2.0);
  new_dts[2] *= 2.0;

  FindAveragedFluxLimiterCounts(system);
//  dt_ = *std::min_element(new_dts, new_dts + 3);
//  thread_->FindMinValue(dt_);
}

Real RungeKutta3StepperWithSplitting::DoAdvectionStep(DynSysSplitting &system,
                                                      std::vector<Real> &system_state,
                                                      Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);
  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateAdvection(system_state, k_1_, k_1_, 0.0, dt);
  thread_->SynchronizeVector(k_1_);
  new_dts[1] = system.CalculateAdvection(system_state, k_1_, k_2_, 1.0, dt);
  thread_->SynchronizeVector(k_2_);
  std::vector<Real> &k_12 = k_2_;
  std::transform(k_1_.begin(), k_1_.end(), k_2_.begin(), k_12.begin(), std::plus<Real>());
  new_dts[2] = system.CalculateAdvection(system_state, k_12, k_3_, 0.25, dt);

  for (int idx : thread_->GetLoopIndices())
  {
    system_state[idx] += (k_12[idx] + 4.0 * k_3_[idx]) * dt / 6.0;
  } // idx
  thread_->SynchronizeVector(system_state);

  return *std::min_element(new_dts.begin(), new_dts.end());
}

Real RungeKutta3StepperWithSplitting::DoSynchronizationStep(DynSysSplitting &system,
                                                            std::vector<Real> &system_state,
                                                            Real dt)
{
  std::vector<Real> new_dts(order_, 0.0);
  // No need to reset k_1_ to 0.0, since k_coef is 0.0
  new_dts[0] = system.CalculateSynchronization(system_state, k_1_, k_1_, 0.0, dt);
  thread_->SynchronizeVector(k_1_);
  new_dts[1] = system.CalculateSynchronization(system_state, k_1_, k_2_, 1.0, dt);
  thread_->SynchronizeVector(k_2_);
  std::vector<Real> &k_12 = k_2_;
  std::transform(k_1_.begin(), k_1_.end(), k_2_.begin(), k_12.begin(), std::plus<Real>());
  new_dts[2] = system.CalculateSynchronization(system_state, k_12, k_3_, 0.25, dt);

  for (int idx : thread_->GetLoopIndices())
  {
    system_state[idx] += (k_12[idx] + 4.0 * k_3_[idx]) * dt / 6.0;
  } // idx
  thread_->SynchronizeVector(system_state);

  return *std::min_element(new_dts.begin(), new_dts.end());
}

void RungeKutta3StepperWithSplitting::FindAveragedFluxLimiterCounts(const DynSysSplitting &system)
{
  average_flux_limiter_count_ = system.GetFluxLimiterCount();
  thread_->ComputeSumIntoRootOnly(average_flux_limiter_count_);
  average_flux_limiter_count_[0] = average_flux_limiter_count_[0] / (2 * order_); // 2 for two advection steps
  average_flux_limiter_count_[1] = average_flux_limiter_count_[1] / (2 * order_);
  average_flux_limiter_count_[2] = average_flux_limiter_count_[2] / (order_);
}