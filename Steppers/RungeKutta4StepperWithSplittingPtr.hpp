//
// Created by Nikita Kruk on 2019-01-03.
//

#ifndef SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPERWITHSPLITTINGPTR_HPP
#define SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPERWITHSPLITTINGPTR_HPP

#include "../Definitions.hpp"
#include "../DynamicalSystems/DynSysSplittingPtr.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"
#include "../DynamicalSystems/VortexArraysSystemSplittingBasicPtr.hpp"

#include <vector>

class RungeKutta4StepperWithSplittingPtr
{
 public:

  explicit RungeKutta4StepperWithSplittingPtr(ThreadXySharedMemory *thread, Real dt);
  ~RungeKutta4StepperWithSplittingPtr();

  Real GetDt() const
  { return dt_; }
  const std::vector<int> &GetAverageFluxLimiterCount() const
  { return average_flux_limiter_count_; }

  void DoStep(DynSysSplittingPtr &system, Real *const system_state);
  void DoStep(VortexArraysSystemSplittingBasicPtr &system, Real *const system_state);

 private:

  ThreadXySharedMemory *thread_;
  Real dt_;
  int order_;
  std::vector<Real> k_1_;
  std::vector<Real> k_2_;
  std::vector<Real> k_3_;
  std::vector<Real> k_4_;
  std::vector<int> average_flux_limiter_count_;

  Real DoAdvectionStep(DynSysSplittingPtr &system,
                       Real *const system_state,
                       Real dt);
  Real DoSynchronizationStep(DynSysSplittingPtr &system,
                             Real *const system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const DynSysSplittingPtr &system);

  Real DoAdvectionStep(VortexArraysSystemSplittingBasicPtr &system,
                       Real *const system_state,
                       Real dt);
  Real DoSynchronizationStep(VortexArraysSystemSplittingBasicPtr &system,
                             Real *const system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const VortexArraysSystemSplittingBasicPtr &system);

};

#endif //SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA4STEPPERWITHSPLITTINGPTR_HPP
