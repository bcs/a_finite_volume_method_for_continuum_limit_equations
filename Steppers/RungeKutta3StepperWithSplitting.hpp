//
// Created by Nikita Kruk on 2018-12-06.
//

#ifndef SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA3STEPPERWITHSPLITTING_HPP
#define SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA3STEPPERWITHSPLITTING_HPP

#include "../Definitions.hpp"
#include "../DynamicalSystems/DynSysSplitting.hpp"
#include "../Parallelization/Thread.hpp"

#include <vector>

class RungeKutta3StepperWithSplitting
{
 public:

  explicit RungeKutta3StepperWithSplitting(Thread *thread, Real dt);
  ~RungeKutta3StepperWithSplitting();

  Real GetDt() const
  { return dt_; }
  const std::vector<int> &GetAverageFluxLimiterCount() const
  { return average_flux_limiter_count_; }

  void DoStep(DynSysSplitting &system, std::vector<Real> &system_state);

 private:

  Thread *thread_;
  Real dt_;
  int order_;
  std::vector<Real> k_1_;
  std::vector<Real> k_2_;
  std::vector<Real> k_3_;
  std::vector<int> average_flux_limiter_count_;

  Real DoAdvectionStep(DynSysSplitting &system,
                       std::vector<Real> &system_state,
                       Real dt);
  Real DoSynchronizationStep(DynSysSplitting &system,
                             std::vector<Real> &system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const DynSysSplitting &system);

};

#endif //SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA3STEPPERWITHSPLITTING_HPP
