//
// Created by Nikita Kruk on 09.12.19.
//

#ifndef SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA2STEPPERWITHSPLITTINGPTR_HPP
#define SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA2STEPPERWITHSPLITTINGPTR_HPP

#include "../Definitions.hpp"
#include "../DynamicalSystems/DynSysSplittingPtr.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"

#include <vector>

class RungeKutta2StepperWithSplittingPtr
{
 public:

  explicit RungeKutta2StepperWithSplittingPtr(ThreadXySharedMemory *thread, Real dt);
  ~RungeKutta2StepperWithSplittingPtr();

  [[nodiscard]] Real GetDt() const
  { return dt_; }
  [[nodiscard]] const std::vector<int> &GetAverageFluxLimiterCount() const
  { return average_flux_limiter_count_; }

  void DoStep(DynSysSplittingPtr &system, Real *const system_state);

 private:

  ThreadXySharedMemory *thread_;
  Real dt_;
  int order_;
  std::vector<Real> k_1_;
  std::vector<Real> k_2_;
  std::vector<int> average_flux_limiter_count_;

  Real DoAdvectionStep(DynSysSplittingPtr &system,
                       Real *const system_state,
                       Real dt);
  Real DoSynchronizationStep(DynSysSplittingPtr &system,
                             Real *const system_state,
                             Real dt);
  void FindAveragedFluxLimiterCounts(const DynSysSplittingPtr &system);

};

#endif //SPC2FINITEVOLUMEMETHODS_RUNGEKUTTA2STEPPERWITHSPLITTINGPTR_HPP
