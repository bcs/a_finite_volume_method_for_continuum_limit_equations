//
// Created by Nikita Kruk on 05.11.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_STRANGSPLITTINGSTEPPER_HPP
#define SPC2FVMNONLINNONLOCEQS_STRANGSPLITTINGSTEPPER_HPP

#include "../Definitions.hpp"
#include "../Parallelization/Thread.hpp"
#include "../DynamicalSystems/DynSysSplitting.hpp"
#include "../DynamicalSystems/DynSysSplittingStructPresFvm.hpp"
#include "RungeKutta4StepperWithSplitting.hpp"
#include "RungeKutta2StepperWithSplitting.hpp"

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm> // std::fill

/**
 * Stepper implements the evolution of a quantity over time,
 * which is decomposed using the Strang splitting for
 * advection (\Delta t / 2), synchronization (\Delta t), and advection (\Delta t / 2) steps,
 * in total, giving an update over \Delta t
 */
class ForwardEulerStepperWithSplitting
{
 public:

  explicit ForwardEulerStepperWithSplitting(Thread &thread, Real dt)
      : thread_(thread), dt_(dt), average_flux_limiter_count_(kDim, 0), order_(1)
  {}
  ~ForwardEulerStepperWithSplitting()
  {}

  Real GetDt() const
  { return dt_; }
  const std::vector<int> &GetAverageFluxLimiterCount() const
  { return average_flux_limiter_count_; }

  template<class T>
  void DoStep(T &system, std::vector<Real> &system_state)
  {
    system.ResetFluxLimiterCount();

    Real new_dt[3] = {0.0};
    new_dt[0] = DoAdvectionStep(system, system_state, dt_ / 2.0);
//    DoExactAdvectionStep(system, system_state, kDt / 2.0);
    new_dt[0] *= 2.0;

    new_dt[1] = DoSynchronizationStep(system, system_state, dt_);

    new_dt[2] = DoAdvectionStep(system, system_state, dt_ / 2.0);
//    DoExactAdvectionStep(system, system_state, kDt / 2.0);
    new_dt[2] *= 2.0;

    FindAveragedFluxLimiterCounts(system);
//    dt_ = *std::min_element(new_dt, new_dt + 3);
//    thread.FindMinValue(dt_);
  }

 private:

  Thread &thread_;
  Real dt_;
  std::vector<int> average_flux_limiter_count_;
  int order_;

  template<class T>
  Real DoAdvectionStep(T &system, std::vector<Real> &system_state, Real dt) const
  {
    static std::vector<Real> flux_x(system_state.size(), (Real) 0.0);
    std::fill(flux_x.begin(), flux_x.end(), 0.0);
    static std::vector<Real> flux_y(system_state.size(), (Real) 0.0);
    std::fill(flux_y.begin(), flux_y.end(), 0.0);

    Real new_dt = system.CalculateAdvection(system_state, flux_x, flux_y, dt);
    thread_.SynchronizeVector(flux_x);
    thread_.SynchronizeVector(flux_y);

    int i = 0, j = 0, k = 0;
    int idx_prev_x = 0, idx_prev_y = 0;
    for (int idx : thread_.GetLoopIndices())
    {
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      utilities::ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev_x);
      utilities::ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev_y);
      system_state[idx] +=
          (-dt / kDx * (flux_x[idx] - flux_x[idx_prev_x])
              - dt / kDy * (flux_y[idx] - flux_y[idx_prev_y]));
    } // idx
    thread_.SynchronizeVector(system_state);

    return new_dt;
  }

  template<class T>
  void DoExactAdvectionStep(T &system, std::vector<Real> &system_state, Real dt) const
  {
#if defined(MULTIPRECISION)
    using boost::multiprecision::sin, boost::multiprecision::cos, boost::multiprecision::floor;
#else
    using std::sin, std::cos, std::floor;
#endif

    static std::vector<Real> new_system_state(system_state);
    std::fill(new_system_state.begin(), new_system_state.end(), 0.0);

    int i = 0, j = 0, k = 0;
    int i_0 = 0, j_0 = 0, idx_0 = 0;
    for (int idx : thread_.GetLoopIndices())
    {
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      i_0 = utilities::PositiveModulo(int(floor((X(i) - dt * cos(Phi(k))) / kDx)), kN);
      j_0 = utilities::PositiveModulo(int(floor((Y(j) - dt * sin(Phi(k))) / kDy)), kM);
//      std::cout << i << "," << j << " <-> " << i_0 << "," << j_0 << std::endl;
      utilities::ThreeDimIdxToOneDimIdx(i_0, j_0, k, idx_0);
      new_system_state[idx] = system_state[idx_0];
    } // idx
    for (int idx : thread_.GetLoopIndices())
    {
      system_state[idx] = new_system_state[idx];
    } // idx
    thread_.SynchronizeVector(system_state);
  }

  template<class T>
  Real DoSynchronizationStep(T &system, std::vector<Real> &system_state, Real dt) const
  {
    static std::vector<Real> flux_phi(system_state.size(), (Real) 0.0);
    std::fill(flux_phi.begin(), flux_phi.end(), 0.0);

    Real new_dt = system.CalculateSynchronization(system_state, flux_phi, dt);
    // No need to synchronize flux_phi, if the whole phi-range is in one thread
//    thread.SynchronizeVector(flux_phi);

    int i = 0, j = 0, k = 0;
    int idx_prev_phi = 0;
    for (int idx : thread_.GetLoopIndices())
    {
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      utilities::ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev_phi);
      system_state[idx] +=
          (-dt / kDphi * (flux_phi[idx] - flux_phi[idx_prev_phi]));
    } // idx
    thread_.SynchronizeVector(system_state);

    return new_dt;
  }

  template<class T>
  void FindAveragedFluxLimiterCounts(const T &system)
  {
    average_flux_limiter_count_ = system.GetFluxLimiterCount();
    thread_.ComputeSumIntoRootOnly(average_flux_limiter_count_);
    average_flux_limiter_count_[0] = average_flux_limiter_count_[0] / (2 * order_); // 2 for two advection steps
    average_flux_limiter_count_[1] = average_flux_limiter_count_[1] / (2 * order_);
    average_flux_limiter_count_[2] = average_flux_limiter_count_[2] / (order_);
  }
};

#endif //SPC2FVMNONLINNONLOCEQS_STRANGSPLITTINGSTEPPER_HPP
