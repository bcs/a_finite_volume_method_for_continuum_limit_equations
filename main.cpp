#include "ControlEngines/SimulationEngine.hpp"
#include "ControlEngines/SimulationEnginePtr.hpp"
#include "Parallelization/Thread.hpp"
#include "Parallelization/ThreadXySharedMemory.hpp"
#include "Parallelization/Parallelization.hpp"

#include <string> // std::stoi

//Real kAlpha = 0.0;
//Real kDiffusionConstant = 0.0;

int main(int argc, char **argv)
{
  /*if (argc > 1)
  {
    kDiffusionConstant = std::stod(argv[1]);
  }
  if (argc > 2)
  {
    kAlpha = std::stod(argv[2]);
  }*/

  LaunchParallelSession(argc, argv);
  {
#if defined(MPI_PARALLELIZATION_IN_XY_TWO_SIDED)
    ThreadXyTwoSided thread(argc, argv);
    SimulationEngine engine(&thread);
#elif defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED)
    ThreadXyOneSided thread(argc, argv);
    SimulationEngine engine(&thread);
#elif defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED)
    ThreadXyOneSidedDistributed thread(argc, argv);
  SimulationEngineDistributed engine(&thread);
#elif defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY)
    ThreadXySharedMemory thread(argc, argv);
    SimulationEnginePtr engine(&thread);
#elif defined(MPI_PARAMETER_SCAN)
    Thread thread(argc, argv);
    SimulationEngine engine(&thread);
#else
    Thread thread(argc, argv);
    SimulationEngine engine(&thread);
#endif
    engine.RunSimulation();
//    engine.RunContinuationMethod();
  }
  FinalizeParallelSession();
  return 0;
}
