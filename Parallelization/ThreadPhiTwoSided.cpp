//
// Created by Nikita Kruk on 2018-12-28.
//

#include "ThreadPhiTwoSided.hpp"
#include "Parallelization.hpp"

#include <mpi.h>
#include <numeric>  // std::iota
#include <cassert>
#include <algorithm> // std::min_element

ThreadPhiTwoSided::ThreadPhiTwoSided(int argc, char **argv) :
    Thread(argc, argv)
{
  root_rank_ = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
  MPI_Comm_size(MPI_COMM_WORLD, &number_of_mpich_threads_);
  assert(!(kL % number_of_mpich_threads_));
  number_of_spatial_elements_per_mpich_thread_ = kL / number_of_mpich_threads_; // TODO: rename the variable
  number_of_elements_per_mpich_thread_ = kN * kM * kL / number_of_mpich_threads_;

  loop_indices_ = std::vector<int>(number_of_elements_per_mpich_thread_, 0);
  std::iota(loop_indices_.begin(), loop_indices_.end(), rank_ * number_of_elements_per_mpich_thread_);
  spatial_loop_indices_ = std::vector<int>(kN * kM, 0);
  std::iota(spatial_loop_indices_.begin(), spatial_loop_indices_.end(), 0);
}

ThreadPhiTwoSided::~ThreadPhiTwoSided()
{

}

bool ThreadPhiTwoSided::IsRoot()
{
  return (rank_ == root_rank_);
}

void ThreadPhiTwoSided::SynchronizeVector(std::vector<Real> &vec)
{
  std::vector<Real> buf(kN * kM * kL, 0.0);

  MPI_Gather(&vec[rank_ * number_of_elements_per_mpich_thread_],
             number_of_elements_per_mpich_thread_,
             kRealTypeForMpi,
             &buf[0],
             number_of_elements_per_mpich_thread_,
             kRealTypeForMpi,
             root_rank_,
             MPI_COMM_WORLD);
  if (rank_ == root_rank_)
  {
    vec.swap(buf);
  }
  MPI_Bcast(&vec[0], (int) vec.size(), kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::SynchronizeSpatialVector(std::vector<Real> &vec)
{
  std::vector<Real> buf(kN * kM, 0.0);

  MPI_Gather(&vec[rank_ * number_of_spatial_elements_per_mpich_thread_],
             number_of_spatial_elements_per_mpich_thread_,
             kRealTypeForMpi,
             &buf[0],
             number_of_spatial_elements_per_mpich_thread_,
             kRealTypeForMpi,
             root_rank_,
             MPI_COMM_WORLD);
  if (rank_ == root_rank_)
  {
    vec.swap(buf);
  }
  MPI_Bcast(&vec[0], (int) vec.size(), kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::BroadcastVector(std::vector<Real> &vec)
{
  MPI_Bcast(&vec[0], (int) vec.size(), kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::BroadcastValue(double &v)
{
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::FindMinValue(double &v)
{
  std::vector<Real> vec((unsigned long) (number_of_mpich_threads_), 0.0);
  MPI_Gather(&v, 1, kRealTypeForMpi, &vec[0], 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    v = *std::min_element(vec.begin(), vec.end());
  }
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::ComputeSumIntoRootOnly(std::vector<int> &vec)
{
  std::vector<int> global_vec(vec.size(), 0);
  MPI_Reduce(&vec[0], &global_vec[0], (int) vec.size(), MPI_INT, MPI_SUM, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    vec.swap(global_vec);
  }
}

void ThreadPhiTwoSided::BroadcastCondition(int &condition)
{
  MPI_Bcast(&condition, 1, MPI_INT, root_rank_, MPI_COMM_WORLD);
}

void ThreadPhiTwoSided::InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices)
{

}