//
// Created by Nikita Kruk on 2019-01-02.
//

#ifndef SPC2FINITEVOLUMEMETHODS_THREADXYONESIDEDSHAREDMEMORY_HPP
#define SPC2FINITEVOLUMEMETHODS_THREADXYONESIDEDSHAREDMEMORY_HPP

#include "Thread.hpp"

#include <mpi.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>

class ThreadXySharedMemory : public Thread
{
 public:

  ThreadXySharedMemory(int argc, char **argv);
  ~ThreadXySharedMemory();

  int GetRootRank();
  int GetSharedRootRank();
  const std::vector<int> &GetSharedRanks();
  const std::vector<int> &GetSharedRootRankIndexes();
  const std::vector<int> &GetNumbersOfSharedMpichThreads();
  MPI_Comm &GetSharedCommunicator();
  MPI_Win &GetWindow(const std::string &window_name);

  bool IsRoot() override;
  bool IsSharedRoot();

  void SynchronizeVector(Dimension dim, std::vector<Real> &vec);
  void SynchronizeVector(std::vector<Real> &vec) override;
  void SynchronizeVector(Real *const vec, long size);
  void SynchronizeVectorThroughoutClusters(Real *const vec);
  void BroadcastVector(std::vector<Real> &vec) override;
  void BroadcastVector(Real *const vec, long size);
  void BroadcastVectorThroughoutClusters(Real *const vec);
  void BroadcastValue(double &v) override;
  void FindMinValue(double &v) override;
  void ComputeSumIntoRootOnly(std::vector<int> &vec) override;
  void BroadcastCondition(int &condition) override;
  void InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices) override;

  void AllocateSharedWindow(MPI_Aint size, Real *&vec, const std::string &window_name);
//  void AllocateSharedWindow(MPI_Aint size, Real *vec, MPI_Win &window);
  void FreeSharedWindow(const std::string &window_name);
//  void FreeSharedWindow(MPI_Win &window);

 private:

  int shared_rank_;
  std::vector<int> shared_ranks_;
  int shared_root_rank_;
  std::vector<int> shared_root_rank_indexes_;
  int number_of_shared_mpich_threads_; // in a shared memory communicator
  std::vector<int> numbers_of_shared_mpich_threads_; // in each shared memory communicator
  MPI_Comm shared_communicator_;
  std::unordered_map<std::string, MPI_Win> windows_;
//  std::unordered_set<int> neighbor_threads_;
  std::unordered_map<int, int> index_to_prev_x_thread_;
  std::unordered_map<int, int> index_to_prev_y_thread_;

};

#endif //SPC2FINITEVOLUMEMETHODS_THREADXYONESIDEDSHAREDMEMORY_HPP
