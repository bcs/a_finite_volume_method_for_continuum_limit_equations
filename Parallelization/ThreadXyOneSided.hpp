//
// Created by Nikita Kruk on 2018-12-28.
//

#ifndef SPC2FINITEVOLUMEMETHODS_THREADXYONESIDED_HPP
#define SPC2FINITEVOLUMEMETHODS_THREADXYONESIDED_HPP

#include "Thread.hpp"

#include <unordered_set>
#include <unordered_map>

class ThreadXyOneSided : public Thread
{
 public:

  ThreadXyOneSided(int argc, char **argv);
  ~ThreadXyOneSided();

  bool IsRoot() override;

  void SynchronizeVector(std::vector<Real> &vec) override;
  void BroadcastVector(std::vector<Real> &vec) override;
  void BroadcastValue(double &v) override;
  void FindMinValue(double &v) override;
  void ComputeSumIntoRootOnly(std::vector<int> &vec) override;
  void BroadcastCondition(int &condition) override;

  void InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices) override;

 private:

  std::unordered_set<int> neighbor_threads_;
  std::unordered_map<int, int> index_to_neighbor_thread_;

};

#endif //SPC2FINITEVOLUMEMETHODS_THREADXYONESIDED_HPP
