//
// Created by Nikita Kruk on 2018-12-28.
//

#include "ThreadXyTwoSided.hpp"
#include "Parallelization.hpp"

#include <mpi.h>
#include <numeric>  // std::iota
#include <cassert>
#include <algorithm> // std::min_element

ThreadXyTwoSided::ThreadXyTwoSided(int argc, char **argv) :
    Thread(argc, argv)
{
  root_rank_ = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
  MPI_Comm_size(MPI_COMM_WORLD, &number_of_mpich_threads_);
  assert(!((kN * kM)
      % number_of_mpich_threads_));  // width by height must be divisible by the number of threads for this kind of parallelization
  number_of_spatial_elements_per_mpich_thread_ = kN * kM / number_of_mpich_threads_;
  number_of_elements_per_mpich_thread_ = kL * kN * kM / number_of_mpich_threads_;

  loop_indices_ = std::vector<int>(number_of_elements_per_mpich_thread_, 0);
  std::iota(loop_indices_.begin(), loop_indices_.end(), rank_ * number_of_elements_per_mpich_thread_);
  spatial_loop_indices_ = std::vector<int>(number_of_spatial_elements_per_mpich_thread_, 0);
  std::iota(spatial_loop_indices_.begin(), spatial_loop_indices_.end(), rank_ * number_of_spatial_elements_per_mpich_thread_);
}

ThreadXyTwoSided::~ThreadXyTwoSided()
{

}

bool ThreadXyTwoSided::IsRoot()
{
  return (rank_ == root_rank_);
}

void ThreadXyTwoSided::SynchronizeVector(std::vector<Real> &vec)
{
  std::vector<Real> buf(kL * kN * kM, 0.0);

  MPI_Gather(&vec[rank_ * number_of_elements_per_mpich_thread_],
             number_of_elements_per_mpich_thread_,
             kRealTypeForMpi,
             &buf[0],
             number_of_elements_per_mpich_thread_,
             kRealTypeForMpi,
             root_rank_,
             MPI_COMM_WORLD);
  if (rank_ == root_rank_)
  {
    vec.swap(buf);
  }
  MPI_Bcast(&vec[0], (int) vec.size(), kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyTwoSided::BroadcastVector(std::vector<Real> &vec)
{
  MPI_Bcast(&vec[0], (int) vec.size(), kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyTwoSided::BroadcastValue(double &v)
{
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyTwoSided::FindMinValue(double &v)
{
  std::vector<Real> vec((unsigned long) (number_of_mpich_threads_), 0.0);
  MPI_Gather(&v, 1, kRealTypeForMpi, &vec[0], 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    v = *std::min_element(vec.begin(), vec.end());
  }
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyTwoSided::ComputeSumIntoRootOnly(std::vector<int> &vec)
{
  std::vector<int> global_vec(vec.size(), 0);
  MPI_Reduce(&vec[0], &global_vec[0], (int) vec.size(), MPI_INT, MPI_SUM, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    vec.swap(global_vec);
  }
}

void ThreadXyTwoSided::BroadcastCondition(int &condition)
{
  MPI_Bcast(&condition, 1, MPI_INT, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyTwoSided::InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices)
{

}