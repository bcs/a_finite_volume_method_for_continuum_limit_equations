//
// Created by Nikita Kruk on 15.07.18.
//

#include "Thread.hpp"

#include <cassert>
#include <iostream>
#include <numeric>  // std::iota
#include <algorithm> // std::min_element

Thread::Thread(int argc, char **argv)
{
  root_rank_ = 0;
  rank_ = 0;
  number_of_mpich_threads_ = 1;
  assert(!((kN * kM)
      % number_of_mpich_threads_));  // width by height must be divisible by the number of threads for this kind of parallelization
  number_of_spatial_elements_per_mpich_thread_ = kN * kM / number_of_mpich_threads_;
  number_of_elements_per_mpich_thread_ = kL * kN * kM / number_of_mpich_threads_;

  loop_indices_ = std::vector<int>(kL * number_of_spatial_elements_per_mpich_thread_, 0);
  std::iota(loop_indices_.begin(), loop_indices_.end(), rank_ * kL * number_of_spatial_elements_per_mpich_thread_);
  spatial_loop_indices_ = std::vector<int>(number_of_spatial_elements_per_mpich_thread_, 0);
  std::iota(spatial_loop_indices_.begin(),
            spatial_loop_indices_.end(),
            rank_ * number_of_spatial_elements_per_mpich_thread_);
}

Thread::~Thread()
{

}

int Thread::GetRank()
{
  return rank_;
}

int Thread::GetNumberOfMpichThreads()
{
  return number_of_mpich_threads_;
}

int Thread::GetNumberOfElementsPerMpichThread()
{
  return number_of_elements_per_mpich_thread_;
}

const std::vector<int> &Thread::GetLoopIndices()
{
  return loop_indices_;
}

const std::vector<int> &Thread::GetSpatialLoopIndices()
{
  return spatial_loop_indices_;
}

bool Thread::IsRoot()
{
  return true;
}

void Thread::SynchronizeVector(std::vector<Real> &vec)
{

}

void Thread::SynchronizeSpatialVector(std::vector<Real> &vec)
{

}

void Thread::BroadcastVector(std::vector<Real> &vec)
{

}

void Thread::BroadcastValue(double &v)
{

}

void Thread::FindMinValue(double &v)
{

}

void Thread::ComputeSumIntoRootOnly(std::vector<int> &vec)
{

}

void Thread::BroadcastCondition(int &condition)
{

}

void Thread::InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices)
{

}