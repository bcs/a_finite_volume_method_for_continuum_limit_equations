//
// Created by Nikita Kruk on 13.07.18.
//

#include "Parallelization.hpp"

#if defined(MPI_PARALLELIZATION_IN_XY_TWO_SIDED) || defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED) || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY)
#include <mpi.h>
#if defined(OPENMP_MIXED_WITH_MPI_PARALLELIZATION_IN_XY_TWO_SIDED)
#include <omp.h>
#endif
#endif

#if defined(MPI_PARALLELIZATION_IN_XY_TWO_SIDED) || defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED) || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY)
void LaunchParallelSession(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
}
#else
void LaunchParallelSession(int argc, char **argv)
{

}
#endif

#if defined(MPI_PARALLELIZATION_IN_XY_TWO_SIDED) || defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED) || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY)
void FinalizeParallelSession()
{
  MPI_Finalize();
}
#else
void FinalizeParallelSession()
{

}
#endif