//
// Created by Nikita Kruk on 2018-12-28.
//

#ifndef SPC2FINITEVOLUMEMETHODS_THREADPHITWOSIDED_HPP
#define SPC2FINITEVOLUMEMETHODS_THREADPHITWOSIDED_HPP

#include "Thread.hpp"

class ThreadPhiTwoSided : public Thread
{
 public:

  ThreadPhiTwoSided(int argc, char **argv);
  ~ThreadPhiTwoSided();

  bool IsRoot() override;

  void SynchronizeVector(std::vector<Real> &vec) override;
  void SynchronizeSpatialVector(std::vector<Real> &vec) override;
  void BroadcastVector(std::vector<Real> &vec) override;
  void BroadcastValue(double &v) override;
  void FindMinValue(double &v) override;
  void ComputeSumIntoRootOnly(std::vector<int> &vec) override;
  void BroadcastCondition(int &condition) override;

  void InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices) override;

};

#endif //SPC2FINITEVOLUMEMETHODS_THREADPHITWOSIDED_HPP
