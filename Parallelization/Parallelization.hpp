//
// Created by Nikita Kruk on 13.07.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_PARALLELIZATION_HPP
#define SPC2FVMNONLINNONLOCEQS_PARALLELIZATION_HPP

#include "../Definitions.hpp"

#if defined(MPI_PARALLELIZATION_IN_XY_TWO_SIDED) || defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED) || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED) \
 || defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY)
#include <mpi.h>
const MPI_Datatype kRealTypeForMpi = MPI_DOUBLE;
#else
const int kRealTypeForMpi = -1;
#endif

void LaunchParallelSession(int argc, char **argv);
void FinalizeParallelSession();

#endif //SPC2FVMNONLINNONLOCEQS_PARALLELIZATION_HPP
