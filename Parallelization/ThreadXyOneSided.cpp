//
// Created by Nikita Kruk on 2018-12-28.
//

#include "ThreadXyOneSided.hpp"
#include "Parallelization.hpp"

#include <mpi.h>
#include <numeric>  // std::iota
#include <cassert>
#include <algorithm> // std::min_element

ThreadXyOneSided::ThreadXyOneSided(int argc, char **argv) :
    Thread(argc, argv)
{
  root_rank_ = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
  MPI_Comm_size(MPI_COMM_WORLD, &number_of_mpich_threads_);
  assert(!((kN * kM)
      % number_of_mpich_threads_));  // width by height must be divisible by the number of threads for this kind of parallelization
  number_of_spatial_elements_per_mpich_thread_ = kN * kM / number_of_mpich_threads_;
  number_of_elements_per_mpich_thread_ = kL * kN * kM / number_of_mpich_threads_;

  loop_indices_ = std::vector<int>(number_of_elements_per_mpich_thread_, 0);
  std::iota(loop_indices_.begin(), loop_indices_.end(), rank_ * number_of_elements_per_mpich_thread_);
  spatial_loop_indices_ = std::vector<int>(number_of_spatial_elements_per_mpich_thread_, 0);
  std::iota(spatial_loop_indices_.begin(),
            spatial_loop_indices_.end(),
            rank_ * number_of_spatial_elements_per_mpich_thread_);
}

ThreadXyOneSided::~ThreadXyOneSided()
{

}

bool ThreadXyOneSided::IsRoot()
{
  return (rank_ == root_rank_);
}

void ThreadXyOneSided::SynchronizeVector(std::vector<Real> &vec)
{
  // v1
//  MPI_Win win;
//  if (rank_ == root_rank_)
//  {
//    MPI_Win_create(&vec[0], vec.size() * sizeof(Real), sizeof(Real), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
//    MPI_Win_fence(0, win);
//    MPI_Win_fence(0, win);
//    MPI_Win_fence(0, win);
//  } else
//  {
//    MPI_Win_create(NULL, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win);
//    MPI_Win_fence(0, win);
//    MPI_Put(&vec[rank_ * number_of_elements_per_mpich_thread_],
//            number_of_elements_per_mpich_thread_,
//            kRealTypeForMpi,
//            root_rank_,
//            rank_ * number_of_elements_per_mpich_thread_,
//            number_of_elements_per_mpich_thread_,
//            kRealTypeForMpi,
//            win);
//    MPI_Win_fence(0, win);
//    MPI_Get(&vec[0], vec.size(), kRealTypeForMpi, root_rank_, 0, vec.size(), kRealTypeForMpi, win);
//    MPI_Win_fence(0, win);
//  }
//  MPI_Win_free(&win);

  // v2
  assert(!neighbor_threads_.empty());
  MPI_Win win;
  std::vector<Real> vec_prev(vec.begin(), vec.end());
  MPI_Win_create(&vec_prev[0], vec_prev.size() * sizeof(Real), sizeof(Real), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
  MPI_Win_fence(0, win);
  for (int neighbor_thread_rank : neighbor_threads_)
  {
    MPI_Get(&vec[neighbor_thread_rank * number_of_elements_per_mpich_thread_],
            number_of_elements_per_mpich_thread_,
            kRealTypeForMpi,
            neighbor_thread_rank,
            neighbor_thread_rank * number_of_elements_per_mpich_thread_,
            number_of_elements_per_mpich_thread_,
            kRealTypeForMpi,
            win);
  } // neighbor_thread_rank
  MPI_Win_fence(0, win);
  MPI_Win_free(&win);

  // v3
//  // TODO: Check the correctness!
//  assert(!index_to_neighbor_thread_.empty());
//  MPI_Win win;
//  std::vector<Real> vec_prev(vec.begin(), vec.end());
//  MPI_Win_create(&vec_prev[0], vec_prev.size() * sizeof(Real), sizeof(Real), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
//  MPI_Win_fence(0, win);
//  for (const std::pair<const int, int> &it: index_to_neighbor_thread_)
//  {
//    MPI_Get(&vec[0 + kL * it.first], kL, kRealTypeForMpi, it.second, 0 + kL * it.first, kL, kRealTypeForMpi, win);
//  } // it
//  MPI_Win_fence(0, win);
//  MPI_Win_free(&win);
}

void ThreadXyOneSided::BroadcastVector(std::vector<Real> &vec)
{
  MPI_Win win;
  if (rank_ == root_rank_)
  {
    MPI_Win_create(&vec[0], vec.size() * sizeof(Real), sizeof(Real), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    MPI_Win_fence(0, win);
    MPI_Win_fence(0, win);
  } else
  {
    MPI_Win_create(nullptr, 0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    MPI_Win_fence(0, win);
    MPI_Get(&vec[0], vec.size(), kRealTypeForMpi, root_rank_, 0, vec.size(), kRealTypeForMpi, win);
    MPI_Win_fence(0, win);
  }
  MPI_Win_free(&win);
}

void ThreadXyOneSided::BroadcastValue(double &v)
{
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyOneSided::FindMinValue(double &v)
{
  std::vector<Real> vec((unsigned long) (number_of_mpich_threads_), 0.0);
  MPI_Gather(&v, 1, kRealTypeForMpi, &vec[0], 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    v = *std::min_element(vec.begin(), vec.end());
  }
  MPI_Bcast(&v, 1, kRealTypeForMpi, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyOneSided::ComputeSumIntoRootOnly(std::vector<int> &vec)
{
  std::vector<int> global_vec(vec.size(), 0);
  MPI_Reduce(&vec[0], &global_vec[0], (int) vec.size(), MPI_INT, MPI_SUM, root_rank_, MPI_COMM_WORLD);
  if (IsRoot())
  {
    vec.swap(global_vec);
  }
}

void ThreadXyOneSided::BroadcastCondition(int &condition)
{
  MPI_Bcast(&condition, 1, MPI_INT, root_rank_, MPI_COMM_WORLD);
}

void ThreadXyOneSided::InitializeNeighborThreads(std::vector<std::vector<int>> &spatial_neighbor_indices)
{
  for (const int &spatial_index: spatial_loop_indices_)
  {
//    int i = spatial_index % kN;
//    int j = spatial_index / kN;
    int k = 0;
    for (const int &spatial_neighbor_index : spatial_neighbor_indices[spatial_index])
    {
      int l = spatial_neighbor_index % kN;
      int m = spatial_neighbor_index / kN;
      int idx = 0;
      utilities::ThreeDimIdxToOneDimIdx(l, m, k, idx);
      int neighbor_thread_rank = idx / number_of_elements_per_mpich_thread_;
      if (neighbor_thread_rank != rank_)
      {
        neighbor_threads_.insert(neighbor_thread_rank);
        index_to_neighbor_thread_[spatial_neighbor_index] = neighbor_thread_rank;
      }
    } // spatial_neighbor_index
  } // spatial_index
}