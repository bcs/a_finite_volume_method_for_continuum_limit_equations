This project contains implementation of finite volume methods developed for the paper ["A Finite Volume Method for Continuum Limit Equations of Nonlocally Interacting Active Chiral Particles" by N. Kruk, J. A. Carrillo, and H. Koeppl](https://arxiv.org/abs/2008.08493).

Examples of the temporal evolution of probability density functions, considered in the aforementioned paper, one can find under https://figshare.com/projects/A_Finite_Volume_Method_for_Continuum_Limit_Equations_of_Nonlocally_Interacting_Active_Chiral_Particles/82166.

Dependencies: [boost](https://www.boost.org/), [MPICH](https://www.mpich.org/), [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page), [GMP](https://gmplib.org/), [MPFR](https://www.mpfr.org/), [MPC](http://www.multiprecision.org/mpc/).
