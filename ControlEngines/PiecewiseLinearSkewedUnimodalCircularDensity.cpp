//
// Created by Nikita Kruk on 17.01.20.
//

#include "PiecewiseLinearSkewedUnimodalCircularDensity.hpp"

#include <fstream>
#include <utility> // std::move
#include <cassert>

PiecewiseLinearSkewedUnimodalCircularDensity::PiecewiseLinearSkewedUnimodalCircularDensity(std::string global_file_name,
                                                                                           int n,
                                                                                           int m,
                                                                                           int l,
                                                                                           int t0) :
    global_file_name_(std::move(global_file_name)),
    num_cells_{n, m, l},
    cell_size_{kXSize / n, kYSize / m, kPhiSize / l},
    numerical_solution_(n * m * l, 0.0),
    numerical_derivatives_(n * m * l, std::array<Real, kDim>{0.0, 0.0, 0.0})
{
  std::ifstream solution_file(global_file_name_, std::ios::in | std::ios::binary);
  assert(solution_file.is_open());
  solution_file.seekg(t0 * (1l + n * m * l) * sizeof(RealForOutput), std::ios::cur);
  auto t = Real(0.0);
  solution_file.read((char *) &t, sizeof(Real));
  solution_file.read((char *) &numerical_solution_[0], n * m * l * sizeof(Real));
  CalculateDerivativesAtCellCenters();
}

PiecewiseLinearSkewedUnimodalCircularDensity::~PiecewiseLinearSkewedUnimodalCircularDensity() = default;

Real PiecewiseLinearSkewedUnimodalCircularDensity::EvaluateAt(Real x, Real y, Real phi) const
{
  int i = 0, j = 0, k = 0, cell_idx = 0;
  i = int((x + 0.5 * cell_size_[0]) / kXSize * num_cells_[0]);
  if (i == num_cells_[0])
  {
    i = 0;
    x -= kXSize;
  }
  j = int((y + 0.5 * cell_size_[1]) / kYSize * num_cells_[1]);
  if (j == num_cells_[1])
  {
    j = 0;
    y -= kYSize;
  }
  k = int((phi + 0.5 * cell_size_[2]) / kPhiSize * num_cells_[2]);
  if (k == num_cells_[2])
  {
    k = 0;
    phi -= kPhiSize;
  }
  utilities::ThreeDimIdxToOneDimIdx(i, j, k, cell_idx, num_cells_[0], num_cells_[1], num_cells_[2]);
  std::array<Real, kDim> cell_center = {i * cell_size_[0], j * cell_size_[1], k * cell_size_[2]};

  return numerical_solution_[cell_idx] + (x - cell_center[0]) * numerical_derivatives_[cell_idx][0]
      + (y - cell_center[1]) * numerical_derivatives_[cell_idx][1]
      + (phi - cell_center[2]) * numerical_derivatives_[cell_idx][2];
}

void PiecewiseLinearSkewedUnimodalCircularDensity::CalculateDerivativesAtCellCenters()
{
  std::array<int, kDim> cell_idx_prev{0, 0, 0}, cell_idx_next{0, 0, 0};
  int i = 0, j = 0, k = 0;
  for (int cell_idx = 0; cell_idx < numerical_solution_.size(); ++cell_idx)
  {
    utilities::OneDimIdxToThreeDimIdx(cell_idx, i, j, k, num_cells_[0], num_cells_[1], num_cells_[2]);
    // neighboring cells wrt x
    utilities::ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, num_cells_[0]),
                                      j,
                                      k,
                                      cell_idx_prev[0],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    utilities::ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, num_cells_[0]),
                                      j,
                                      k,
                                      cell_idx_next[0],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    CalculateDerivativeInOneDimension(cell_idx,
                                      cell_idx_prev[0],
                                      cell_idx_next[0],
                                      cell_size_[0],
                                      numerical_derivatives_[cell_idx][0]);
    // neighboring cells wrt y
    utilities::ThreeDimIdxToOneDimIdx(i,
                                      utilities::PositiveModulo(j - 1, num_cells_[1]),
                                      k,
                                      cell_idx_prev[1],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    utilities::ThreeDimIdxToOneDimIdx(i,
                                      utilities::PositiveModulo(j + 1, num_cells_[1]),
                                      k,
                                      cell_idx_next[1],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    CalculateDerivativeInOneDimension(cell_idx,
                                      cell_idx_prev[1],
                                      cell_idx_next[1],
                                      cell_size_[1],
                                      numerical_derivatives_[cell_idx][1]);
    // neighboring cells wrt phi
    utilities::ThreeDimIdxToOneDimIdx(i,
                                      j,
                                      utilities::PositiveModulo(k - 1, num_cells_[2]),
                                      cell_idx_prev[2],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    utilities::ThreeDimIdxToOneDimIdx(i,
                                      j,
                                      utilities::PositiveModulo(k + 1, num_cells_[2]),
                                      cell_idx_next[2],
                                      num_cells_[0],
                                      num_cells_[1],
                                      num_cells_[2]);
    CalculateDerivativeInOneDimension(cell_idx,
                                      cell_idx_prev[2],
                                      cell_idx_next[2],
                                      cell_size_[2],
                                      numerical_derivatives_[cell_idx][2]);
  } // cell_idx
}

void PiecewiseLinearSkewedUnimodalCircularDensity::CalculateDerivativeInOneDimension(int cell_idx,
                                                                                     int cell_idx_prev,
                                                                                     int cell_idx_next,
                                                                                     Real cell_size,
                                                                                     Real &numerical_derivative)
{
  numerical_derivative =
      (numerical_solution_[cell_idx_next] - numerical_solution_[cell_idx_prev]) / (Real(2.0) * cell_size);
  Real numerical_solution_at_next_cell_interface =
      numerical_solution_[cell_idx] + Real(0.5) * cell_size * numerical_derivative;
  Real numerical_solution_at_prev_cell_interface =
      numerical_solution_[cell_idx] - Real(0.5) * cell_size * numerical_derivative;
  if ((numerical_solution_at_next_cell_interface < 0.0) || (numerical_solution_at_prev_cell_interface < 0.0))
  {
    numerical_derivative =
        utilities::Minmod((numerical_solution_[cell_idx_next] - numerical_solution_[cell_idx]) / cell_size,
                          numerical_derivative,
                          (numerical_solution_[cell_idx] - numerical_solution_[cell_idx_prev]) / cell_size);
  }
}