//
// Created by Nikita Kruk on 2019-01-02.
//

#ifndef SPC2FINITEVOLUMEMETHODS_SIMULATIONENGINEPTR_HPP
#define SPC2FINITEVOLUMEMETHODS_SIMULATIONENGINEPTR_HPP

#include "../Definitions.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"

class SimulationEnginePtr
{
 public:

  explicit SimulationEnginePtr(ThreadXySharedMemory *thread);
  ~SimulationEnginePtr();

  void RunSimulation();
  void RunContinuationMethod();

 private:

  ThreadXySharedMemory *thread_;
  Real *system_state_;
  long system_state_size_;

  void InitializeSystemStateByRule();
  void InitializeSystemStateFromFile(bool should_add_perturbation);
  void InitializeSystemStateFromParticleDynamics();

};

#endif //SPC2FINITEVOLUMEMETHODS_SIMULATIONENGINEPTR_HPP
