//
// Created by Nikita Kruk on 12.06.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_SIMULATIONENGINE_HPP
#define SPC2FVMNONLINNONLOCEQS_SIMULATIONENGINE_HPP

#include "../Definitions.hpp"
#include "../Parallelization/Thread.hpp"

#include <vector>

class SimulationEngine
{
 public:

  explicit SimulationEngine(Thread *thread);
  ~SimulationEngine();

  void SaveInitialCondition();
  void RunSimulation();

 private:

  Thread *thread_;
  std::vector<Real> system_state_;

  void InitializeSystemStateByRule();
  void InitializeSystemStateFromFile(bool should_add_perturbation);

};

#endif //SPC2FVMNONLINNONLOCEQS_SIMULATIONENGINE_HPP
