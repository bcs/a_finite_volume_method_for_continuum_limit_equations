//
// Created by Nikita Kruk on 12.06.18.
//

#include "SimulationEngine.hpp"
#include "../Observers/BinaryObserver.hpp"
#include "../DynamicalSystems/DynSysSplittingBasic.hpp"
#include "../DynamicalSystems/DynSysSplittingFast.hpp"
#include "../DynamicalSystems/DynSysSplittingFastMc.hpp"
#include "../Steppers/RungeKutta2StepperWithSplitting.hpp"
#include "../Steppers/RungeKutta4StepperWithSplitting.hpp"
#include "../DynamicalSystems/VortexArraysSystemSplittingBasic.hpp"
#include "InitialConditions.hpp"

#include <iostream>
#include <random>
#include <algorithm>  // std::for_each
#include <fstream>
#include <cassert>

SimulationEngine::SimulationEngine(Thread *thread) :
    system_state_((unsigned long) (kN * kM * kL), 0.0),
    thread_(thread)
{

}

SimulationEngine::~SimulationEngine()
{
  system_state_.clear();
}

void SimulationEngine::InitializeSystemStateByRule()
{
  if (thread_->IsRoot())
  {
//    InitialConditions::UniformPerturbed<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::QuasiUniformInPhi<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::QuasiUniformInXYPhi<std::vector<Real>>(system_state_, system_state_.size(), 1.0, false);
//    InitialConditions::DiracDelta<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::SynchronizedPlane<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::PhiSinWave<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::XySinWave<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::DiscOrCylinder<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::ThreeDimGaussianDensity<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::TwoDimGaussianDensity<std::vector<Real>>(system_state_, system_state_.size());
//    InitialConditions::OneDimGaussianDensity<std::vector<Real>>(system_state_, system_state_.size());

    Real overall_density = std::accumulate(system_state_.begin(), system_state_.end(), Real(0.0));
    overall_density *= kDx * kDy * kDphi;
    std::for_each(system_state_.begin(), system_state_.end(), [&](Real &ss) { ss /= overall_density; });

//    SaveInitialCondition();
  }
//#if defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED)
//  MPI_Win_fence(0, thread_.GetSystemStateWindow());
//  if (!thread_.IsRoot())
//  {
//    MPI_Get(&system_state_[0],
//            system_state_.size(),
//            kRealTypeForMpi,
//            0,
//            0,
//            system_state_.size(),
//            kRealTypeForMpi,
//            thread_.GetSystemStateWindow());
//  }
//  MPI_Win_fence(0, thread_.GetSystemStateWindow());
//#else
  thread_->BroadcastVector(system_state_);
//#endif
}

void SimulationEngine::InitializeSystemStateFromFile(bool should_add_perturbation)
{
  if (thread_->IsRoot())
  {
#if defined(__linux__) && defined(BCS_CLUSTER)
    std::ifstream init_cond_file("/home/nkruk/cpp/spc2FiniteVolumeMethods/output/dt_0.001_sigma_1_rho_0_alpha_1.54_Dphi_0_40_40_128_1000_v0.bin",
         std::ios::in | std::ios::binary);
#elif defined(__APPLE__)
    std::ifstream init_cond_file
        ("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/dt_0.01_sigma_4_rho_0.3_alpha_1.54_Dphi_0.01_20_20_128_1000_from_random.bin",
         std::ios::in | std::ios::binary);
#endif
    assert(init_cond_file.is_open());
    int t_init = 18, dt_recip = 1;
    init_cond_file.seekg(t_init * dt_recip * (1l + system_state_.size()) * sizeof(RealForOutput), std::ios::cur);
    RealForOutput t = 0.0;
    init_cond_file.read((char *) &t, sizeof(RealForOutput));
    std::vector<RealForOutput> float_system_state(system_state_.size(), RealForOutput(0.0));
    init_cond_file.read((char *) &float_system_state[0], kN * kM * kL * sizeof(RealForOutput));
    init_cond_file.close();
    if (!should_add_perturbation)
    {
      std::copy(float_system_state.begin(), float_system_state.end(), system_state_.begin());
    } else
    {
      InitialConditions::QuasiUniformInXYPhi<std::vector<Real>>(system_state_, system_state_.size());
      std::transform(system_state_.begin(),
                     system_state_.end(),
                     float_system_state.begin(),
                     system_state_.begin(),
                     [](Real x, RealForOutput y) -> Real { return x + y; });
      Real overall_density = std::accumulate(system_state_.begin(), system_state_.end(), Real(0));
      overall_density *= kDx * kDy * kDphi;
      std::for_each(system_state_.begin(), system_state_.end(), [&](Real &ss) { ss /= overall_density; });
    }
  }
//#if defined(MPI_PARALLELIZATION_IN_XY_ONE_SIDED)
//  MPI_Win_fence(0, thread_.GetSystemStateWindow());
//#else
  thread_->BroadcastVector(system_state_);
//#endif
}

void SimulationEngine::SaveInitialCondition()
{
  std::string input_folder("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/Continuation/");
  std::string input_file_name("dt_0.005_sigma_1_rho_0.3_alpha_1.45_Dphi_0.0075_40_40_256_1000.bin");
  std::ifstream prev_simulation_file(input_folder + input_file_name, std::ios::in | std::ios::binary);
  assert(prev_simulation_file.is_open());
  prev_simulation_file.seekg(4000 * 1 * (1l + kN * kM * kL) * sizeof(RealForOutput), std::ios::beg);
  RealForOutput t = 0.0;
  prev_simulation_file.read((char *) &t, sizeof(RealForOutput));
  prev_simulation_file.read((char *) &system_state_[0], kN * kM * kL * sizeof(RealForOutput));
  prev_simulation_file.close();

  std::ofstream init_cond_file(input_folder + "init_cond_" + input_file_name,
                               std::ios::out | std::ios::binary | std::ios::trunc);
  assert(init_cond_file.is_open());
  t = 0.0;
  init_cond_file.write((char *) &t, sizeof(RealForOutput));
  init_cond_file.write((char *) &system_state_[0], system_state_.size() * sizeof(RealForOutput));
  init_cond_file.close();
}

#if defined(MPI_PARAMETER_SCAN)
#include<mpi.h>
#endif
void SimulationEngine::RunSimulation()
{
  if (thread_->IsRoot())
  {
    std::cout << "simulation started with " << thread_->GetNumberOfMpichThreads() << " (MPICH) threads" << std::endl;
  }

#if defined(MPI_PARAMETER_SCAN)
  int rank = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  //kAlpha = 1.5;
  //kDiffusionConstant = 0.0025 + rank * 0.0025;
  kDiffusionConstant = 0.4;
  kAlpha = 0.0 + rank * 0.01;
#endif

  InitializeSystemStateByRule();
//  InitializeSystemStateFromFile(false);
  BinaryObserver observer(thread_);
  RungeKutta2StepperWithSplitting stepper(thread_, kDt);
//  RungeKutta4StepperWithSplitting stepper(thread_, kDt);
  DynSysSplittingFast system(thread_);
//  VortexArraysSystemSplittingBasic system(thread_);

  Real t = kT0;
  observer.SaveSystemState(system_state_, t);
  while (t <= kT1)
  {
    t += stepper.GetDt();
    stepper.DoStep(system, system_state_);
    observer.SaveSystemState(system_state_, t);
    observer.SaveSummaryStatistics(system_state_, t);
    observer.SaveAdditionalInformation(stepper.GetAverageFluxLimiterCount(),
                                       system.GetCflA(),
                                       system.GetCflB(),
                                       system.GetCflC(),
                                       t);
    thread_->BroadcastCondition(observer.should_terminate_);
    if (observer.should_terminate_)
    {
      break;
    }
  }

  if (thread_->IsRoot())
  {
    std::cout << "simulation ended" << std::endl;
  }
}