//
// Created by Nikita Kruk on 10.12.19.
//

#ifndef SPC2FINITEVOLUMEMETHODS_INITIALCONDITIONS_HPP
#define SPC2FINITEVOLUMEMETHODS_INITIALCONDITIONS_HPP

#include "../Definitions.hpp"
#include "SkewedUnimodalCircularDensity.hpp"
#include "PiecewiseLinearSkewedUnimodalCircularDensity.hpp"

#include <vector>
#include <random>
#include <iostream>
#include <algorithm> // std::min_element, std::transform
#include <array>
#include <boost/math/special_functions/bessel.hpp>
#include <eigen3/Eigen/Dense>

class InitialConditions
{
 public:

  template<typename T>
  static void UniformPerturbed(T &vec, long size)
  {
    std::mt19937 mersenne_twister_generator(std::random_device{}());
    std::uniform_real_distribution<Real> unif_real_dist(-0.0001, 0.0001);
    for (int idx = 0; idx < size; ++idx)
    {
      vec[idx] = 1.0 / (2.0 * M_PI) + unif_real_dist(mersenne_twister_generator);
    } // idx
    std::cout << typeid(vec).name() << std::endl;
  }

  // Dirac delta function
  template<typename T>
  static void DiracDelta(T &vec, long size)
  {
    int i = 0;
    int j = 0;
    int k = 19;
    int idx = 0;
    utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
    vec[idx] = 1.0;
  }

  // f(x,y,\varphi,t)=\delta(\varphi-\varphi_0)
  template<typename T>
  static void SynchronizedPlane(T &vec, long size)
  {
    for (int i = 0; i < kN; ++i)
    {
      for (int j = 0; j < kM; ++j)
      {
        int idx = 0;
        for (int k = 1; k < kL; ++k)
        {
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          vec[idx] = 0.0;
        }
        int k = 0;
        utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
        vec[idx] = 1.0 / kDphi;
      } // j
    } // i
  }

  // sin-wave in \varphi, uniform in (x,y)
  template<typename T>
  static void PhiSinWave(T &vec, long size)
  {
    for (int i = 0; i < kN; ++i)
    {
      for (int j = 0; j < kM; ++j)
      {
        for (int k = 0; k < kL; ++k)
        {
          int idx = 0;
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          // strong positivity is required for the logarithmic representation of diffusion
          vec[idx] = 2.0 + std::sin(Phi(k));
        } // k
      } // j
    } // i
  }

  // double sin-wave in (x,y), uniform in \varphi
  template<typename T>
  static void XySinWave(T &vec, long size)
  {
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
//      if (0 == k)
      vec[idx] = 2.0 + std::sin(2.0 * M_PI * X(i)) + std::sin(2.0 * M_PI * Y(j));
    } // idx
  }

  // von Mises distribution in \varphi, uniform in (x,y)
  template<typename T>
  static void OneDimVonMisesDensity(T &vec, long size)
  {
    Real mu = 0.0;
    Real gamma = 1.0;
    for (int idx = 0; idx < size; ++idx)
    {
//      std::uniform_real_distribution<Real> uni_real_dist(-0.01, 0.01);
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = std::exp(gamma * std::cos(Phi(k) - mu)) / (2.0 * M_PI * boost::math::cyl_bessel_i(0, gamma));
    } // idx
  }

  // von Mises distibution in (x,y), uniform in \varphi
  template<typename T>
  static void TwoDimVonMisesDensity(T &vec, long size)
  {
    Real k1 = 1.0, k2 = 1.0;
    Real mu1 = 0.0, mu2 = 0.0;
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = std::exp(k1 * std::cos(2.0 * M_PI * (X(i) - mu1)) + k2 * std::cos(2.0 * M_PI * (Y(j) - mu2)))
          / (boost::math::cyl_bessel_i(0, k1) * boost::math::cyl_bessel_i(0, k2));
    } // idx
  }

  // wrapped Cauchy distribution in \varphi, uniform in (x,y)
  template<typename T>
  static void WrappedCauchyDensity(T &vec, long size)
  {
    Real mu = M_PI;
    Real gamma = 0.4;
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = 1.0 / (2.0 * M_PI) * std::sinh(gamma) / (std::cosh(gamma) - std::cos(Phi(k) - mu));
    } // idx
  }

  // sine-skewed Cauchy distribution in (x,y,\varphi)
  template<typename T>
  static void SineSkewedCauchyDensity(T &vec, long size)
  {
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      Real rho_0 = 0.68, mu = M_PI, lambda = 0.5;
      vec[idx] =
          1.0 / (2.0 * M_PI) * (1.0 - rho_0 * rho_0) / (1.0 + rho_0 * rho_0 - 2.0 * rho_0 * std::cos(Phi(k) - mu))
              * (1.0 + lambda * std::sin(1.0 * (Phi(k) - mu)));
      rho_0 = 0.25;
      mu = 0.5;
      lambda = 0.04;
      vec[idx] *=
          (1.0 - rho_0 * rho_0) / (1.0 + rho_0 * rho_0 - 2.0 * rho_0 * std::cos(2.0 * M_PI * (X(i) - mu)))
              * (1.0 + lambda * std::sin(2.0 * M_PI * 1 * (X(i) - mu)));
      vec[idx] *=
          (1.0 - rho_0 * rho_0) / (1.0 + rho_0 * rho_0 - 2.0 * rho_0 * std::cos(2.0 * M_PI * (Y(j) - mu)))
              * (1.0 + lambda * std::sin(2.0 * M_PI * 1 * (Y(j) - mu)));
    } // idx
  }

  // splay state
  template<typename T>
  static void SplayState(T &vec, long size)
  {
    for (int i = 0; i < kN; ++i)
    {
      for (int j = 0; j < kM; ++j)
      {
        int num_of_states = 2;
        int dist_between_states = kL / num_of_states;
//        for (int k = 0; k < num_of_states; ++k)
//        {
//          int idx = 0;
//          ThreeDimIdxToOneDimIdx(i, j, k * dist_between_states, idx);
//          vec[idx] = 1.0;
//        } // k
        for (int k = 0; k < kL; ++k)
        {
          int idx = 0;
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          for (int n = 0; n < num_of_states; ++n)
          {
            vec[idx] += std::exp(std::cos(Phi(k) - n * dist_between_states * kDphi));
          } // n
        } // k
      } // j
    } // i
  }

  // a disc in (x,y)
  template<typename T>
  static void DiscOrCylinder(T &vec, long size)
  {
    for (int i = 0; i < kN; ++i)
    {
      for (int j = 0; j < kM; ++j)
      {
        int k = 0;
//        for (int k = 0; k < kL; ++k)
        {
          int idx = 0;
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          if (utilities::ClassCPeriodicBoundaryDistance(0.0, 0.0, X(i), Y(j)) <= kRho)
          {
            vec[idx] = 1.0;
          } else
          {
            vec[idx] = 0.0;
          }
        }
      } // j
    } // i
  }

  // Gaussian density in (x,y,\varphi)
  template<typename T>
  static void ThreeDimGaussianDensity(T &vec, long size)
  {
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);

      Eigen::Vector3d state(X(i), Y(j), Phi(k));
      Eigen::Vector3d mean(0.5, 0.5, M_PI);
      Real sigma = 0.25;
      Eigen::Matrix3d covariance;
      covariance << sigma * sigma, 0.0, 0.0,
          0.0, sigma * sigma, 0.0,
          0.0, 0.0, sigma * sigma;
      vec[idx] = 0.1 + std::exp(-0.5 * (state - mean).transpose() * covariance.inverse().eval() * (state - mean))
          / (std::pow(2.0 * M_PI, 1.5) * std::sqrt(covariance.determinant()));
//      vec[idx] = 1.0 + 1.0 / std::sqrt(2.0 * M_PI * sigma * sigma)
//          * std::exp(-0.5 * (Phi(k) - mean(2)) * (Phi(k) - mean(2)) / (sigma * sigma));
    } // idx
  }

  // Gaussian density in (x,y)
  template<typename T>
  static void TwoDimGaussianDensity(T &vec, long size)
  {
    for (int i = 0; i < kN; ++i)
    {
      for (int j = 0; j < kM; ++j)
      {
        std::array<Real, 2> state = {X(i), Y(j)};
        std::array<Real, 2> mean = {0.0, 0.0};
        std::array<Real, 2> diff = {0.0, 0.0};
        utilities::ClassCPeriodicBoundaryDistance(state[0], state[1], mean[0], mean[1], diff[0], diff[1]);
//        ApplyShiftedPeriodicBoundaryContitions(diff[0]], diff[1]], diff[0]], diff[1]]);
        Real sigma = 0.2;
        Eigen::Vector2d x(diff[0], diff[1]);
        Eigen::Matrix2d covariance;
        covariance << sigma * sigma, 0.0, 0.0, sigma * sigma;
        for (int k = 0; k < kL; ++k)
        {
          int idx = 0;
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          vec[idx] = 0.1 + std::exp(-0.5 * x.transpose() * covariance.inverse().eval() * x)
              / (std::pow(2.0 * M_PI, 1.0) * std::sqrt(covariance.determinant()));
        } // k
      } // j
    } // i
  }

  // Gaussian density in \varphi, uniform in (x,y)
  template<typename T>
  static void OneDimGaussianDensity(T &vec, long size)
  {
    for (int k = 0; k < kL; ++k)
    {
      Real mean = M_PI;
      Real sigma = 0.1;
      for (int i = 0; i < kN; ++i)
      {
        for (int j = 0; j < kM; ++j)
        {
          int idx = 0;
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          vec[idx] = std::exp(-0.5 * (Phi(k) - mean) * (Phi(k) - mean) / sigma / sigma)
              / std::sqrt(2.0 * M_PI * sigma * sigma);
        } // j
      } // i
    } // k
  }

  // Quasi uniform in \varphi, uniform in (x,y)
  template<typename T>
  static void QuasiUniformInPhi(T &vec, long size)
  {
    std::mt19937 mersenne_twister_generator(std::random_device{}());
    std::uniform_real_distribution<Real> unif_real_dist(-0.0001, 0.0001);
    const size_t num_modes = 20;
    std::vector<Real> a(num_modes, 0.0), b(num_modes, 0.0);
    for (int k = 0; k < num_modes; ++k)
    {
      a[k] = unif_real_dist(mersenne_twister_generator);
      b[k] = unif_real_dist(mersenne_twister_generator);
    } // k
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = 1.0;
      for (int mode = 1; mode <= num_modes; ++mode)
      {
        vec[idx] += a[mode - 1] * std::cos(mode * Phi(k)) + b[mode - 1] * std::sin(mode * Phi(k));
      } // mode
    } // idx
//    Real min_value = *std::min_element(&vec[0], &vec[size]);
//    std::transform(&vec[0], &vec[size], &vec[0], [&](Real r) { return r - min_value; });
  }

  // Quasi uniform in (x,y), uniform in \varphi
  template<typename T>
  static void QuasiUniformInXY(T &vec,
                               long size,
                               Real base = 0.0,
                               Real amplitude = 1e-02,
                               bool is_positivity_required = false)
  {
    // the seed should be constexpr for reproducibility
    auto seed = std::random_device{}();
    std::mt19937 mersenne_twister_generator(seed);
    std::uniform_real_distribution<Real> unif_real_dist(0.0, 2.0 * M_PI);
    const size_t num_modes_x = 10, num_modes_y = 10;
    std::vector<std::vector<Real>> a(num_modes_x, std::vector<Real>(num_modes_y, 0.0));
    std::vector<std::vector<Real>> b(num_modes_x, std::vector<Real>(num_modes_y, 0.0));
    for (int k_x = 0; k_x < num_modes_x; ++k_x)
    {
      for (int k_y = 0; k_y < num_modes_y; ++k_y)
      {
        a[k_x][k_y] = unif_real_dist(mersenne_twister_generator);
        b[k_x][k_y] = unif_real_dist(mersenne_twister_generator);
      } // k_y
    } // k_x
    Real eps = 1.0 / (num_modes_x * num_modes_y);
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = base;
      for (int k_x = 1; k_x <= num_modes_x; ++k_x)
      {
        for (int k_y = 1; k_y <= num_modes_y; ++k_y)
        {
          vec[idx] += eps * std::sin(2.0 * M_PI * k_x * X(i) - a[k_x - 1][k_y - 1])
              * std::sin(2.0 * M_PI * k_y * Y(j) - b[k_x - 1][k_y - 1]);
          /*vec[idx] += eps * (-1.0) / (4.0 * M_PI * M_PI * k_x * k_y)
              * (std::sin(2.0 * M_PI * k_x * X(i + 0.5) - a[k_x - 1][k_y - 1])
                  - std::sin(2.0 * M_PI * k_x * X(i - 0.5) - a[k_x - 1][k_y - 1]))
              * (std::sin(2.0 * M_PI * k_y * Y(j + 0.5) - b[k_x - 1][k_y - 1])
                  - std::sin(2.0 * M_PI * k_y * Y(j - 0.5) - b[k_x - 1][k_y - 1])) / (kDx * kDy);*/
        } // k_y
      } // k_x
    } // idx
    // if positivity is required
    if (is_positivity_required)
    {
      Real min_value = *std::min_element(&vec[0], &vec[size]), max_value = *std::max_element(&vec[0], &vec[size]);
      std::for_each(&vec[0], &vec[size], [&](Real &x) { x = (x - min_value) / (max_value - min_value) * amplitude; });
    } else
    {
      Real variation = *std::max_element(&vec[0], &vec[size]) - base;
      std::for_each(&vec[0], &vec[size], [&](Real &v) { v = base + (v - base) / variation * amplitude; });
    }
  }

  // Quasi uniform in (x,y,\varphi)
  template<typename T>
  static void QuasiUniformInXYPhi(T &vec,
                                  long size,
                                  Real base = 0.0,
                                  Real amplitude = 1e-02,
                                  bool is_positivity_required = false)
  {
    // the seed should be constexpr for reproducibility
    auto seed = std::random_device{}();
    std::mt19937 mersenne_twister_generator(seed);
    std::uniform_real_distribution<Real> unif_real_dist(0.0, 2.0 * M_PI);
    const size_t num_modes_x = 10, num_modes_y = 10, num_modes_phi = 10;
    std::vector<std::vector<std::vector<Real>>>
        a(num_modes_phi, std::vector<std::vector<Real>>(num_modes_x, std::vector<Real>(num_modes_y, 0.0)));
    std::vector<std::vector<std::vector<Real>>>
        b(num_modes_phi, std::vector<std::vector<Real>>(num_modes_x, std::vector<Real>(num_modes_y, 0.0)));
    std::vector<std::vector<std::vector<Real>>>
        c(num_modes_phi, std::vector<std::vector<Real>>(num_modes_x, std::vector<Real>(num_modes_y, 0.0)));
    for (int k_phi = 0; k_phi < num_modes_phi; ++k_phi)
    {
      for (int k_x = 0; k_x < num_modes_x; ++k_x)
      {
        for (int k_y = 0; k_y < num_modes_y; ++k_y)
        {
          a[k_phi][k_x][k_y] = unif_real_dist(mersenne_twister_generator);
          b[k_phi][k_x][k_y] = unif_real_dist(mersenne_twister_generator);
          c[k_phi][k_x][k_y] = unif_real_dist(mersenne_twister_generator);
        } // k_y
      } // k_x
    } // k_phi
    Real eps = 1.0 / (num_modes_x * num_modes_y * num_modes_phi); // *0.5*amplitude
    for (int idx = 0; idx < size; ++idx)
    {
      int i = 0, j = 0, k = 0;
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = base;
      for (int k_phi = 1; k_phi <= num_modes_phi; ++k_phi)
      {
        for (int k_x = 1; k_x <= num_modes_x; ++k_x)
        {
          for (int k_y = 1; k_y <= num_modes_y; ++k_y)
          {
            vec[idx] += eps * std::sin(2.0 * M_PI * k_x * X(i) - a[k_phi - 1][k_x - 1][k_y - 1])
                * std::sin(2.0 * M_PI * k_y * Y(j) - b[k_phi - 1][k_x - 1][k_y - 1])
                * std::sin(k_phi * Phi(k) - c[k_phi - 1][k_x - 1][k_y - 1]);
            /*vec[idx] += eps * (-1.0) / (4.0 * M_PI * M_PI * k_x * k_y * k_phi)
                * (std::sin(2.0 * M_PI * k_x * X(i + 0.5) - a[k_phi - 1][k_x - 1][k_y - 1])
                    - std::sin(2.0 * M_PI * k_x * X(i - 0.5) - a[k_phi - 1][k_x - 1][k_y - 1]))
                * (std::sin(2.0 * M_PI * k_y * Y(j + 0.5) - b[k_phi - 1][k_x - 1][k_y - 1])
                    - std::sin(2.0 * M_PI * k_y * Y(j - 0.5) - b[k_phi - 1][k_x - 1][k_y - 1]))
                * (std::sin(k_phi * Phi(k + 0.5) - c[k_phi - 1][k_x - 1][k_y - 1])
                    - std::sin(k_phi * Phi(k - 0.5) - c[k_phi - 1][k_x - 1][k_y - 1])) / (kDx * kDy * kDphi);*/
          } // k_y
        } // k_x
      } // k_phi
    } // idx
    // if positivity is required
    if (is_positivity_required)
    {
      Real min_value = *std::min_element(&vec[0], &vec[size]), max_value = *std::max_element(&vec[0], &vec[size]);
      std::for_each(&vec[0], &vec[size], [&](Real &x) { x = (x - min_value) / (max_value - min_value) * amplitude; });
    } else
    {
      Real variation = *std::max_element(&vec[0], &vec[size]) - base;
      std::for_each(&vec[0], &vec[size], [&](Real &v) { v = base + (v - base) / variation * amplitude; });
    }
  }

  // Skewed unimodal circular density function, uniform in (x,y)
  template<typename T>
  static void SkewedUnimodalCircularDensityInPhi(T &vec, long size)
  {
//    Real order_parameter = 0.69572776697380989, group_velocity = -0.45741958487405515; // (a) 1; 0.01; 0.78; 0.2075
//    Real order_parameter = 0.6703717256811248, group_velocity = -0.5086037726606758; // (b) 1; 0.01; 0.9; 0.18
//    Real order_parameter = 0.65225607156087073, group_velocity = -0.66358059591390184; // (c) 1; 0.01; 1.3; 0.06
//    Real order_parameter = 0.80923466366702257, group_velocity = -0.8119303842747384; // (d) 1; 0.01; 1.45; 0.01
//    Real order_parameter = 0.88057380830996623, group_velocity = -0.87209481233404373; // (e) 1; 0.4; 1.45; 0.005
//    Real order_parameter = 0.95954569057031758, group_velocity = -0.92819705302166211; // (f) 1; 0.2; 1.36;0.005
//    Real order_parameter = 0.88567950969803433, group_velocity = -0.83658330867712982; // (g) 1; 0.01; 1.3; 0.02
//    Real order_parameter = 0.95013741545904218, group_velocity = -0.76868475080561471; // (h) 1; 0.01; 1; 0.0375
//    Real order_parameter = 0.9074434313781965, group_velocity = -0.7230242843173208; // (i) 1; 0.01; 1; 0.0575
//    Real order_parameter = 0.58501514086505457, group_velocity = -0.5470739074953469; // (j) 1; 0.01; 1.07; 0.145
//    Real order_parameter = 0.77875063897796748, group_velocity = -3.2068203594744649; // 4; 0.3; 1.54; 0.01
//    Real order_parameter = 0.77875063897196717, group_velocity = -0.80170508986445921; // 1; 0.3; 1.54; 0.0025
    Real order_parameter = 0.55169000313302508, group_velocity = -0.65133050366020351;
    SkewedUnimodalCircularDensity density(kSigma, kAlpha, kDiffusionConstant, group_velocity, order_parameter);
    int idx = 0;
    for (int k = 0; k < kL; ++k)
    {
      Real f_k = density.EvaluateAt(Phi(k));
      for (int i = 0; i < kN; ++i)
      {
        for (int j = 0; j < kM; ++j)
        {
          utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
          vec[idx] = f_k;
        } // j
      } // i
    } // k
  }

  // Piecewise linear reconstruction from another numerical solution
  template<typename T>
  static void PiecewiseLinearDensity(T &vec, long size)
  {
    const int n = 40, m = 40, l = 128;
#if defined(BCS_CLUSTER)
    PiecewiseLinearSkewedUnimodalCircularDensity piecewise_linear_density(std::string(
        "/home/nkruk/cpp/spc2FiniteVolumeMethods/input/PerturbedPiecewiseLinearSkewedUnimodalCircularDensity/"
        "dt_0.01_sigma_4_rho_0.3_alpha_1.54_Dphi_0.01_") + std::to_string(n) + "_" + std::to_string(m) + "_"
                                                                              + std::to_string(l)
                                                                              + std::string("_1000_relaxationtime.bin"), n, m, l, 200);
#else
    PiecewiseLinearSkewedUnimodalCircularDensity piecewise_linear_density
        (std::string("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/"
                     "InitialConditions/PerturbedPiecewiseLinearSkewedUnimodalCircularDensity/"
                     "dt_0.01_sigma_4_rho_0.3_alpha_1.54_Dphi_0.01_")
             + std::to_string(n) + "_" + std::to_string(m) + "_" + std::to_string(l)
             + std::string("_1000_relaxationtime.bin"), n, m, l, 200);
#endif
    int i = 0, j = 0, k = 0;
    for (int idx = 0; idx < size; ++idx)
    {
      utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
      vec[idx] = piecewise_linear_density.EvaluateAt(X(i), Y(j), Phi(k));
    } // idx
  }

};

#endif //SPC2FINITEVOLUMEMETHODS_INITIALCONDITIONS_HPP
