//
// Created by Nikita Kruk on 17.01.20.
//

#ifndef SPC2FINITEVOLUMEMETHODS_PIECEWISELINEARSKEWEDUNIMODALCIRCULARDENSITY_HPP
#define SPC2FINITEVOLUMEMETHODS_PIECEWISELINEARSKEWEDUNIMODALCIRCULARDENSITY_HPP

#include "../Definitions.hpp"

#include <vector>
#include <array>
#include <string>

class PiecewiseLinearSkewedUnimodalCircularDensity
{
 public:

  explicit PiecewiseLinearSkewedUnimodalCircularDensity(std::string global_file_name, int n, int m, int l, int t0 = 0);
  ~PiecewiseLinearSkewedUnimodalCircularDensity();

  [[nodiscard]] Real EvaluateAt(Real x, Real y, Real phi) const;

 private:

  std::string global_file_name_;
  std::array<int, kDim> num_cells_;
  std::array<Real, kDim> cell_size_;
  std::vector<Real> numerical_solution_;
  std::vector<std::array<Real, kDim>> numerical_derivatives_;

  void CalculateDerivativesAtCellCenters();
  void CalculateDerivativeInOneDimension(int cell_idx,
                                         int cell_idx_prev,
                                         int cell_idx_next,
                                         Real cell_size,
                                         Real &numerical_derivative);

};

#endif //SPC2FINITEVOLUMEMETHODS_PIECEWISELINEARSKEWEDUNIMODALCIRCULARDENSITY_HPP
