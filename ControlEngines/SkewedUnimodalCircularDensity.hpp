//
// Created by Nikita Kruk on 07.01.20.
//

#ifndef SPC2FINITEVOLUMEMETHODS_SKEWEDUNIMODALCIRCULARDENSITY_HPP
#define SPC2FINITEVOLUMEMETHODS_SKEWEDUNIMODALCIRCULARDENSITY_HPP

#include "../Definitions.hpp"

#include <boost/multiprecision/mpfr.hpp>

typedef boost::multiprecision::number<boost::multiprecision::mpfr_float_backend<0>> MultiprecisionReal;

class SkewedUnimodalCircularDensity
{
 public:

  explicit SkewedUnimodalCircularDensity(Real coupling,
                                         Real lag,
                                         Real diffusion,
                                         Real group_velocity,
                                         Real order_parameter);
  ~SkewedUnimodalCircularDensity();

  [[nodiscard]] Real EvaluateAt(Real x) const;

 private:

  Real coupling_;
  Real lag_;
  Real diffusion_;
  Real group_velocity_;
  Real order_parameter_;

  int number_of_quadrature_points_;
  std::vector<Real> abstract_quadrature_points_;
  std::vector<Real> abstract_quadrature_weights_;
  int number_of_grid_points_;
  std::vector<Real> decomposed_grid_points_;
  std::vector<MultiprecisionReal> exponent_at_decomposed_point_;
  std::vector<MultiprecisionReal> inverse_exponent_at_decomposed_point_;
  std::vector<MultiprecisionReal> integral_up_to_grid_point_;
  std::vector<MultiprecisionReal> integral_up_to_decomposed_point_;
  MultiprecisionReal normalization_constant_;

  void E(Real x, MultiprecisionReal &res) const;
  void E(const MultiprecisionReal &x, MultiprecisionReal &res) const;
  void EInv(Real x, MultiprecisionReal &res) const;
  void EInv(const MultiprecisionReal &x, MultiprecisionReal &res) const;
  void IntegrateInverseExponentForGrid(Real lower_limit, Real upper_limit);
  void ComputeDensityFunctionNormalizationConstant();
  void CalculateAuxiliaryExponentsAndIntegrals();

};

#endif //SPC2FINITEVOLUMEMETHODS_SKEWEDUNIMODALCIRCULARDENSITY_HPP
