//
// Created by Nikita Kruk on 2019-01-02.
//

#include "SimulationEnginePtr.hpp"
#include "../DynamicalSystems/DynSysSplittingFastPtr.hpp"
#include "../Steppers/RungeKutta2StepperWithSplittingPtr.hpp"
#include "../Steppers/RungeKutta4StepperWithSplittingPtr.hpp"
#include "../Observers/BinaryObserverPtr.hpp"
#include "../Observers/BinaryObserverPtrForContinuationMethod.hpp"
#include "InitialConditions.hpp"

#include <mpi.h>
#include <string>
#include <random>
#include <iostream>
#include <cassert>
#include <boost/math/special_functions/bessel.hpp>

SimulationEnginePtr::SimulationEnginePtr(ThreadXySharedMemory *thread) :
    thread_(thread),
    system_state_size_(kN * kM * kL)
{
  /*if (thread_->IsSharedRoot())
  {
    thread_->AllocateSharedWindow(system_state_size_, system_state_, std::string("system_state_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
  } else
  {
    thread->AllocateSharedWindow(0, system_state_, std::string("system_state_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
    MPI_Aint size;
    int disp_unit;
    MPI_Win_shared_query(thread_->GetWindow(std::string("system_state_window")),
                         thread_->GetSharedRootRank(),
                         &size,
                         &disp_unit,
                         &system_state_);
    assert(system_state_size_ == (size / sizeof(Real)));
  }*/
  system_state_ = new Real[system_state_size_];
}

SimulationEnginePtr::~SimulationEnginePtr()
{
  delete[] system_state_;
  /*MPI_Barrier(thread_->GetSharedCommunicator());
  thread_->FreeSharedWindow(std::string("system_state_window"));*/
}

void SimulationEnginePtr::InitializeSystemStateByRule()
{
  /*MPI_Win &win = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win);*/
  if (thread_->IsRoot())
  {
//    InitialConditions::UniformPerturbed<Real *const>(system_state_, system_state_size_);
//    InitialConditions::QuasiUniformInXY<Real *const>(system_state_, system_state_size_, 1.0 / (2.0 * M_PI));
    InitialConditions::QuasiUniformInXYPhi<Real *const>(system_state_, system_state_size_, 1.0);
//    InitialConditions::SynchronizedPlane<Real *const>(system_state_, system_state_size_);
//    InitialConditions::PhiSinWave<Real *const>(system_state_, system_state_size_);
//    InitialConditions::XySinWave<Real *const>(system_state_, system_state_size_);
//    InitialConditions::DiscOrCylinder<Real *const>(system_state_, system_state_size_);
//    InitialConditions::OneDimVonMisesDensity<Real *const>(system_state_, system_state_size_);
//    InitialConditions::TwoDimVonMisesDensity<Real *const>(system_state_, system_state_size_);
//    InitialConditions::WrappedCauchyDensity<Real *const>(system_state_, system_state_size_);
//    InitialConditions::SineSkewedCauchyDensity<Real *const>(system_state_, system_state_size_);
//    InitialConditions::SkewedUnimodalCircularDensityInPhi(system_state_, system_state_size_);
//    InitialConditions::SplayState<Real *const>(system_state_, system_state_size_);
//    InitialConditions::PiecewiseLinearDensity(system_state_, system_state_size_);

//    std::vector<Real> vec1(system_state_size_, 0.0), vec2(system_state_size_, 0.0);
//    InitialConditions::SkewedUnimodalCircularDensityInPhi(vec1, system_state_size_);
//    Real variation =
//        0.01 * (*std::max_element(vec1.begin(), vec1.end()) - *std::min_element(vec1.begin(), vec1.end()));
//    InitialConditions::QuasiUniformInXY(vec2, system_state_size_, 0.0, variation, true);
//    std::transform(vec1.begin(), vec1.end(), vec2.begin(), &system_state_[0], [](Real v1, Real v2) { return v1 + v2; });

    // normalize the probability density function
    Real overall_density = std::accumulate(&system_state_[0], &system_state_[system_state_size_], Real(0.0));
    overall_density *= kDx * kDy * kDphi;
    std::for_each(&system_state_[0], &system_state_[system_state_size_], [&](Real &ss) { ss /= overall_density; });
  }
  /*MPI_Win_sync(win);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win);
  thread_->BroadcastVectorThroughoutClusters(system_state_);*/

  thread_->BroadcastVector(system_state_, system_state_size_);
}

void SimulationEnginePtr::InitializeSystemStateFromFile(bool should_add_perturbation)
{
  if (thread_->IsRoot())
  {
#if defined(__linux__) && defined(BCS_CLUSTER)
    std::ifstream init_cond_file("/home/nkruk/cpp/spc2FiniteVolumeMethods/input/init_cond_dt_0.005_sigma_1_rho_0.3_alpha_1.45_Dphi_0.0075_40_40_256_1000.bin",
         std::ios::in | std::ios::binary);
#elif defined(__APPLE__)
    std::ifstream init_cond_file
        ("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/Continuation/ContinuationPhaseLagFromNonhomogeneous/dt_0.005_sigma_1_rho_0.3_alpha_1.33_Dphi_0.0075_40_40_256_1000.bin",
         std::ios::in | std::ios::binary);
#endif
    assert(init_cond_file.is_open());
    int t_init = 200, dt_recip = 1;
    init_cond_file.seekg(t_init * dt_recip * (1l + system_state_size_) * sizeof(RealForOutput), std::ios::cur);
    RealForOutput t = 0.0;
    init_cond_file.read((char *) &t, sizeof(RealForOutput));
    std::vector<RealForOutput> float_system_state(system_state_size_, RealForOutput(0.0));
    init_cond_file.read((char *) &float_system_state[0], kN * kM * kL * sizeof(RealForOutput));
    init_cond_file.close();
    if (!should_add_perturbation)
    {
      std::copy(float_system_state.begin(), float_system_state.end(), &system_state_[0]);
    } else
    {
      InitialConditions::QuasiUniformInXYPhi<Real *const>(system_state_, system_state_size_);
      std::transform(&system_state_[0],
                     &system_state_[system_state_size_],
                     float_system_state.begin(),
                     &system_state_[0],
                     [](Real x, RealForOutput y) -> Real { return x + y; });
      Real overall_density = std::accumulate(&system_state_[0], &system_state_[system_state_size_], Real(0.0));
      overall_density *= kDx * kDy * kDphi;
      std::for_each(&system_state_[0], &system_state_[system_state_size_], [&](Real &ss) { ss /= overall_density; });
    }
  }
  thread_->BroadcastVector(system_state_, system_state_size_);
}

void SimulationEnginePtr::InitializeSystemStateFromParticleDynamics()
{
  if (thread_->IsRoot())
  {
#if defined(__linux__) && defined(BCS_CLUSTER)
    std::string folder("/home/nkruk/cpp/spc2OdeIntegration/output/continued/");
    std::ifstream particle_dynamics_file(folder + "v0_0.01_sigma_1_rho_0.01_alpha_0.78_Dphi_0.2075_N_50000_0_0.bin",
        std::ios::binary | std::ios::in);
#elif defined(__APPLE__)
    std::string folder("/Volumes/Kruk/spc2/spc2OdeIntegration/continued/");
    std::ifstream particle_dynamics_file
        (folder + "v0_0.01_sigma_1_rho_0.01_alpha_0.78_Dphi_0.2075_N_50000_0_0.bin", std::ios::binary | std::ios::in);
#endif
    assert(particle_dynamics_file.is_open());
    const int n = 50000, s = 3;
    const int skip_time = 0;
    particle_dynamics_file.seekg(skip_time * (1l + s * n) * sizeof(float), std::ios::cur);
    std::vector<float> particle_solution(s * n, 0.0f);
    float time = 0.0f;
    particle_dynamics_file.read((char *) &time, sizeof(float));
    particle_dynamics_file.read((char *) &particle_solution[0], s * n * sizeof(float));
    particle_dynamics_file.close();

    for (int i = 0; i < n; ++i)
    {
      Real x_i = particle_solution[s * i];
      x_i -= std::floor(x_i * kXRSize) * kXSize;
      Real y_i = particle_solution[s * i + 1];
      y_i -= std::floor(y_i * kYRSize) * kYSize;
      Real phi_i = particle_solution[s * i + 2];
      phi_i -= std::floor(phi_i * kPhiRSize) * kPhiSize;

      int cell_idx_x = int(x_i / kXSize * kN);
      int cell_idx_y = int(y_i / kYSize * kM);
      int cell_idx_phi = int(phi_i / kPhiSize * kL);
      int cell_idx = 0;
      utilities::ThreeDimIdxToOneDimIdx(cell_idx_x, cell_idx_y, cell_idx_phi, cell_idx);
      system_state_[cell_idx] += 1.0 / (n * kDx * kDy * kDphi);
    } // i
    std::for_each(&system_state_[0], &system_state_[system_state_size_], [](Real &ss) { ss += 1.0 / (2.0 * M_PI); });
    Real overall_density = std::accumulate(&system_state_[0], &system_state_[system_state_size_], Real(0.0));
    overall_density *= kDx * kDy * kDphi;
    std::for_each(&system_state_[0], &system_state_[system_state_size_], [&](Real &ss) { ss /= overall_density; });
    std::cout << "initialization complete" << std::endl;
  }
  thread_->BroadcastVector(system_state_, system_state_size_);
}

void SimulationEnginePtr::RunSimulation()
{
  if (thread_->IsRoot())
  {
    std::cout << "simulation started with " << thread_->GetNumberOfMpichThreads() << " (MPICH) threads" << std::endl;
  }

  InitializeSystemStateByRule();
//  InitializeSystemStateFromFile(false);
  BinaryObserverPtr observer(thread_);
  DynSysSplittingFastPtr system(thread_);
  RungeKutta2StepperWithSplittingPtr stepper(thread_, kDt);
/*  BinaryObserverPtr observer(thread_, kVelocity, kMuPlus, kMuMinus, kXiA, kXiR, kDiffusionConstant, kKappa, kRho);
  VortexArraysSystemSplittingBasicPtr system(thread_);*/

  Real t = kT0;
  observer.SaveSystemState(system_state_, system_state_size_, t);
  while (t <= kT1)
  {
    t += stepper.GetDt();
    stepper.DoStep(system, system_state_);
    observer.SaveSystemState(system_state_, system_state_size_, t);
    observer.SaveSummaryStatistics(system_state_, system_state_size_, t);
    observer.SaveAdditionalInformation(stepper.GetAverageFluxLimiterCount(),
                                       system.GetCflA(),
                                       system.GetCflB(),
                                       system.GetCflC(),
                                       t);
    thread_->BroadcastCondition(observer.should_terminate_);
    if (observer.should_terminate_)
    {
      break;
    }
  }

  if (thread_->IsRoot())
  {
    std::cout << "simulation ended" << std::endl;
  }
}

void SimulationEnginePtr::RunContinuationMethod()
{
  /*if (thread_->IsRoot())
  {
    std::cout << "continuation method started with " << thread_->GetNumberOfMpichThreads() << " (MPICH) threads"
              << std::endl;
  }

  Real initial_alpha = 1.335, initial_diffusion = 0.0075;
  kAlpha = initial_alpha;
  kDiffusionConstant = initial_diffusion;
  InitializeSystemStateFromFile(false);
  for (int i_alpha = 1; i_alpha <= 30; ++i_alpha)
  // for (int i_diffusion = 1; i_diffusion <= 20; ++i_diffusion)
  {
    kAlpha = initial_alpha - 0.005 * i_alpha;
    // kDiffusionConstant = initial_diffusion + 0.000625 * i_diffusion;
    BinaryObserverPtrForContinuationMethod observer(thread_, system_state_, system_state_size_);
    DynSysSplittingFastPtr system(thread_);
    RungeKutta2StepperWithSplittingPtr stepper(thread_, kDt);

    Real t = kT0;
    observer.SaveSystemState(system_state_, system_state_size_, t);
    while (true)//(t <= kT1)
    {
      t += stepper.GetDt();
      stepper.DoStep(system, system_state_);
      observer.SaveSystemState(system_state_, system_state_size_, t);
      observer.SaveSummaryStatistics(system_state_, system_state_size_, t);
      observer.SaveAdditionalInformation(stepper.GetAverageFluxLimiterCount(),
                                         system.GetCflA(),
                                         system.GetCflB(),
                                         system.GetCflC(),
                                         t);
      observer.ConsiderSpatialDeviations(system_state_, system_state_size_, t);
      thread_->BroadcastCondition(observer.should_terminate_);
      if (observer.should_terminate_)
      {
        break;
      }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    thread_->BroadcastVector(system_state_, system_state_size_);
  }

  if (thread_->IsRoot())
  {
    std::cout << "continuation method ended" << std::endl;
  }*/
}