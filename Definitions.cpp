//
// Created by Nikita Kruk on 12.06.18.
//

#include "Definitions.hpp"

#include <vector>

namespace utilities
{
  int PositiveModulo(int i, int n)
  {
    return (i % n + n) % n;
//  return (i + n) % n;
  }

  void ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx)
  {
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
    // the winding order is x->y->phi
  idx = x + kN * (y + kM * phi);
#else
    // the winding order is phi->x->y
    idx = phi + kL * (x + kN * y);
#endif
  }

  void ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx, int num_cells_x, int num_cells_y, int num_cells_phi)
  {
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
    // the winding order is x->y->phi
  idx = x + kN * (y + kM * phi);
#else
    // the winding order is phi->x->y
    idx = phi + num_cells_phi * (x + num_cells_x * y);
#endif
  }

  void OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi)
  {
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
    // the winding order is x->y->phi
  phi = idx / (kN * kM);
  y = (idx % (kN * kM)) / kN;
  x = idx % kN;
#else
    // the winding order is phi->x->y
    y = idx / (kL * kN);
    x = (idx % (kL * kN)) / kL;
    phi = idx % kL;
#endif
  }

  void OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi, int num_cells_x, int num_cells_y, int num_cells_phi)
  {
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
    // the winding order is x->y->phi
  phi = idx / (kN * kM);
  y = (idx % (kN * kM)) / kN;
  x = idx % kN;
#else
    // the winding order is phi->x->y
    y = idx / (num_cells_phi * num_cells_x);
    x = (idx % (num_cells_phi * num_cells_x)) / num_cells_phi;
    phi = idx % num_cells_phi;
#endif
  }

  Real Minmod(const Real &a, const Real &b, const Real &c)
  {
    if (a > 0.0 && b > 0.0 && c > 0.0)
    {
      return std::min(std::min(a, b), c);
    } else if (a < 0.0 && b < 0.0 && c < 0.0)
    {
      return std::max(std::max(a, b), c);
    } else
    {
      return Real(0.0);
    }
  }

  Real ClassAPeriodicBoundaryDistance(const Real &x_i, const Real &y_i, const Real &x_j, const Real &y_j)
  {
#if defined(MULTIPRECISION)
    using namespace boost::multiprecision;
#else
    using namespace std;
#endif

    Real dx = x_j - x_i;
    dx -= static_cast<int>(dx * Real(2) * kXRSize) * kXSize;

    Real dy = y_j - y_i;
    dy -= static_cast<int>(dy * Real(2) * kYRSize) * kYSize;

    return hypot(dx, dy);
  }

  void ClassAPeriodicBoundaryDistance(const Real &x_i,
                                      const Real &y_i,
                                      const Real &x_j,
                                      const Real &y_j,
                                      Real &dx,
                                      Real &dy)
  {
    dx = x_j - x_i;
    dx -= static_cast<int>(dx * Real(2) * kXRSize) * kXSize;

    dy = y_j - y_i;
    dy -= static_cast<int>(dy * Real(2) * kYRSize) * kYSize;
  }

// calculate remainders at any distance
//if the sign of the distance is not relevant
  Real ClassCPeriodicBoundaryDistance(const Real &x_i, const Real &y_i, const Real &x_j, const Real &y_j)
  {
#if defined(MULTIPRECISION)
    using namespace boost::multiprecision;
#else
    using namespace std;
#endif

    Real dx = fabs(x_j - x_i);
    dx -= static_cast<int>(dx * kXRSize + 0.5) * kXSize;

    Real dy = fabs(y_j - y_i);
    dy -= static_cast<int>(dy * kYRSize + 0.5) * kYSize;

    return hypot(dx, dy);
  }

  void ClassCPeriodicBoundaryDistance(const Real &x_i,
                                      const Real &y_i,
                                      const Real &x_j,
                                      const Real &y_j,
                                      Real &dx,
                                      Real &dy)
  {
#if defined(MULTIPRECISION)
    using namespace boost::multiprecision;
#else
    using namespace std;
#endif

    // sign is not relevant
//  x_pbc = std::fabs(x_j - x_i);
//  x_pbc -= static_cast<int>(x_pbc * kXRSize + 0.5) * kXSize;
//
//  y_pbc = std::fabs(y_j - y_i);
//  y_pbc -= static_cast<int>(y_pbc * kYRSize + 0.5) * kYSize;

    // sign is relevant
    dx = x_j - x_i;
    dx -= kXSize * nearbyint(dx * kXRSize);

    dy = y_j - y_i;
    dy -= kYSize * nearbyint(dy * kYRSize);
  }

  void ApplyPeriodicBoundaryConditions(const Real &x, const Real &y, Real &x_pbc, Real &y_pbc)
  {
#if defined(MULTIPRECISION)
    using namespace boost::multiprecision;
#else
    using namespace std;
#endif
    x_pbc = x - floor(x * kXRSize) * kXSize;
    y_pbc = y - floor(y * kYRSize) * kYSize;
  }

  void GenerateGaussLegendrePoints(const Real &lower_limit,
                                   const Real &upper_limit,
                                   int n,
                                   std::vector<Real> &x,
                                   std::vector<Real> &w)
  {
#if defined(MULTIPRECISION)
    using namespace boost::multiprecision;
#else
    using namespace std;
#endif

    const double epsilon = 3.0e-11;
    int n_half = 0, j = 0, i = 0;
    Real middle = 0.0, half_length = 0.0, root = 0.0, root_prev = 0.0;
    Real p_1 = 0.0, p_2 = 0.0, p_3 = 0.0, polynomial_derivative = 0.0;

    n_half = (n + 1) / 2;
    middle = 0.5 * (lower_limit + upper_limit);
    half_length = 0.5 * (upper_limit - lower_limit);
    for (i = 0; i < n_half; ++i)
    {
      root = std::cos(M_PI * (i + 0.75) / (n + 0.5));
      do
      {
        p_1 = 1.0;
        p_2 = 0.0;
        for (j = 0; j < n; ++j)
        {
          p_3 = p_2;
          p_2 = p_1;
          // $(j + 1) P_{j + 1} = (2j + 1)xP_j - jP_{j-1}$
          p_1 = ((Real(2) * j + Real(1)) * root * p_2 - j * p_3) / (j + 1);
        } // j
        polynomial_derivative = n * (root * p_1 - p_2) / (root * root - Real(1));
        root_prev = root;
        root = root_prev - p_1 / polynomial_derivative;
      } while (fabs(root - root_prev) > epsilon);
      x[i] = middle - half_length * root;
      x[n - 1 - i] = middle + half_length * root;
      w[i] = Real(2) * half_length / ((Real(1) - root * root) * polynomial_derivative * polynomial_derivative);
      w[n - 1 - i] = w[i];
    } // i
  }
} // namespace utilities

/**
 *
 * @param i
 * @return x_i is the grid value in the middle of a cell, x_{i-0.5} and x_{i+0.5} are the values on the cell interface
 * 0 = x_{-0.5} \equiv x_{N-0.5} = 1
 */
Real X(const Real &i)
{
  return i * kDx;
}

Real Y(const Real &j)
{
  return j * kDx;
}

Real Phi(const Real &k)
{
  return k * kDphi;
}