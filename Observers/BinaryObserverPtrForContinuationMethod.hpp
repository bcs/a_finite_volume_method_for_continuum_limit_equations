//
// Created by Nikita Kruk on 29.04.20.
//

#ifndef SPC2FINITEVOLUMEMETHODS_BINARYOBSERVERPTRFORCONTINUATIONMETHOD_HPP
#define SPC2FINITEVOLUMEMETHODS_BINARYOBSERVERPTRFORCONTINUATIONMETHOD_HPP

#include "../Definitions.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"
#include "BinaryObserverPtr.hpp"

#include <list>

enum class BifurcationPath
{
  kNone, kFromHomogeneous, kFromNonhomogeneousWithSmoothing, kFromNonhomogeneousWithoutSmoothing
};

class BinaryObserverPtrForContinuationMethod : public BinaryObserverPtr
{
 public:

  explicit BinaryObserverPtrForContinuationMethod(ThreadXySharedMemory *thread, Real *const system_state, long size);

  void ConsiderSpatialDeviations(Real *const system_state, long size, Real t);

 private:

  const Real equilibration_time_ = kT1 / 2;
  const int averaging_window_ = kT1 / 4;
  const int frequency_of_perturbation_imposition_ = 100;
  const Real min_smoothing_radius_ = 0.05;

  int time_counter_for_system_analysis_;
  int time_counter_threshold_for_system_analysis_;
  std::list<Real> time_points_;
  std::list<Real> maximum_absolute_spatial_deviations_;
  bool spatial_perturbations_converged_;
  int time_counter_for_perturbation_imposition_;
  BifurcationPath bifurcation_path_;
  int number_of_imposed_smoothings_;

  void ComputeMaximumAbsoluteSpatialDeviation(const Real *const system_state, long size, Real t);
  void GetMeanDerivative(Real &mean_derivative);
  void GetLinearFit(Real &slope, Real &shift);
  static void AddPerturbations(Real *const system_state, long size);
  void SmoothOut(Real *const system_state, long size) const;
  void ClearTemporalStatistics();

};

#endif //SPC2FINITEVOLUMEMETHODS_BINARYOBSERVERPTRFORCONTINUATIONMETHOD_HPP
