//
// Created by Nikita Kruk on 2019-01-04.
//

#include "BinaryObserverPtr.hpp"

#include <mpi.h>
//#include <mpio.h>
#include <sstream>
#include <cassert>
#include <vector>
#include <iostream>
#include <numeric>  // std::accumulate
#include <complex>
#include <iterator> // std::back_inserter, std::copy

BinaryObserverPtr::BinaryObserverPtr(ThreadXySharedMemory *thread) :
    thread_(thread),
    output_time_counter_{0, 0, 0},
    output_time_threshold_{1 * kInverseDt, kInverseDt, kInverseDt}, // mod 1 - save at every dt
    should_terminate_(0)
{
  if (thread->IsRoot())
  {
    integration_step_timer_ = std::chrono::system_clock::now();

#if defined(__linux__) && defined(LICHTENBERG)
    std::string folder("/work/scratch/nk59zoce/cpp/spc2FiniteVolumeMethods/");
#elif defined(__linux__) && defined(BCS_CLUSTER)
    std::string folder("/home/nkruk/cpp/spc2FiniteVolumeMethods/output/ContinuationPhaseLagFromNonhomogeneous/");
#elif defined(__linux__)
    std::string folder("/home/nikita/Documents/spc2FiniteVolumeMethods/");
#elif defined(__APPLE__)
    std::string folder("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/");
#endif
    std::ostringstream output_file_name_buffer;
    output_file_name_buffer << folder << "dt_" << kDt << "_sigma_" << kSigma << "_rho_" << kRho << "_alpha_" << kAlpha
                            << "_Dphi_" << kDiffusionConstant << "_" << kN << "_" << kM << "_" << kL
                            << "_" << kMc << ".bin";
    output_file_name_ = output_file_name_buffer.str();
    if (thread->IsRoot())
    {
      std::remove(output_file_name_.c_str());
    }
    output_file_.open(output_file_name_, std::ios::binary | std::ios::out | std::ios::app);
    assert(output_file_.is_open());

    std::ostringstream summary_statistics_file_name_buffer;
    summary_statistics_file_name_buffer << folder << "dt_" << kDt << "_sigma_" << kSigma << "_rho_" << kRho << "_alpha_"
                                        << kAlpha
                                        << "_Dphi_" << kDiffusionConstant << "_" << kN << "_" << kM << "_" << kL
                                        << "_" << kMc << ".txt";
    summary_statistics_file_name_ = summary_statistics_file_name_buffer.str();
    if (thread->IsRoot())
    {
      std::remove(summary_statistics_file_name_.c_str());
    }
    summary_statistics_file_.open(summary_statistics_file_name_, std::ios::out | std::ios::app);
    assert(summary_statistics_file_.is_open());

    std::ostringstream flux_limiter_activity_file_name_buffer;
    flux_limiter_activity_file_name_buffer << folder << "flux_limiter_dt_" << kDt << "_sigma_" << kSigma << "_rho_"
                                           << kRho << "_alpha_" << kAlpha << "_Dphi_" << kDiffusionConstant << "_" << kN
                                           << "_" << kM << "_" << kL << "_" << kMc << ".txt";
    flux_limiter_activity_file_name_ = flux_limiter_activity_file_name_buffer.str();
    if (thread->IsRoot())
    {
      std::remove(flux_limiter_activity_file_name_.c_str());
    }
    flux_limiter_activity_file_.open(flux_limiter_activity_file_name_, std::ios::out | std::ios::app);
    assert(flux_limiter_activity_file_.is_open());
  }
  MPI_Barrier(MPI_COMM_WORLD);
}

BinaryObserverPtr::BinaryObserverPtr(ThreadXySharedMemory *thread,
                                     Real velocity,
                                     Real mu_plus,
                                     Real mu_minus,
                                     Real xi_a,
                                     Real xi_r,
                                     Real diffusion_constant,
                                     Real kappa,
                                     Real rho) :
    thread_(thread),
    output_time_counter_{0, 0, 0},
    output_time_threshold_{100, 100, 100}, // mod 1 - save at every dt
    should_terminate_(0)
{
//  if (thread->IsRoot())
//  {
  integration_step_timer_ = std::chrono::system_clock::now();

#if defined(__linux__) && defined(LICHTENBERG)
  std::string folder("/work/scratch/nk59zoce/cpp/spc2FiniteVolumeMethods/");
#elif defined(__linux__) && defined(BCS_CLUSTER)
  std::string folder("/home/nkruk/cpp/spc2FiniteVolumeMethods/output/");
#elif defined(__linux__)
  std::string folder("/home/nikita/Documents/spc2FiniteVolumeMethods/");
#elif defined(__APPLE__)
  std::string folder("/Users/nikita/Documents/Projects/spc2/spc2FiniteVolumeMethods/");
#endif
  std::ostringstream output_file_name_buffer;
  output_file_name_buffer << folder << "dt_" << kDt << "_v_" << velocity << "_muplus_" << mu_plus << "_muminus_"
                          << mu_minus << "_xia_" << xi_a << "_xir_" << xi_r << "_Dphi_" << diffusion_constant
                          << "_kappa_" << kappa << "_rho_" << rho << "_" << kN << "_" << kM << "_" << kL << ".bin";
  output_file_name_ = output_file_name_buffer.str();
  if (thread->IsRoot())
  {
    std::remove(output_file_name_.c_str());
  }
  output_file_.open(output_file_name_, std::ios::binary | std::ios::out | std::ios::app);
  assert(output_file_.is_open());

  std::ostringstream summary_statistics_file_name_buffer;
  summary_statistics_file_name_buffer << folder << "dt_" << kDt << "_v_" << velocity << "_muplus_" << mu_plus
                                      << "_muminus_" << mu_minus << "_xia_" << xi_a << "_xir_" << xi_r << "_Dphi_"
                                      << diffusion_constant << "_kappa_" << kappa << "_rho_" << rho << "_" << kN << "_"
                                      << kM << "_" << kL << ".txt";
  summary_statistics_file_name_ = summary_statistics_file_name_buffer.str();
  if (thread->IsRoot())
  {
    std::remove(summary_statistics_file_name_.c_str());
  }
  summary_statistics_file_.open(summary_statistics_file_name_, std::ios::out | std::ios::app);
  assert(summary_statistics_file_.is_open());

  std::ostringstream flux_limiter_activity_file_name_buffer;
  flux_limiter_activity_file_name_buffer << folder << "flux_limiter_dt_" << kDt << "_v_" << velocity << "_muplus_"
                                         << mu_plus << "_muminus_" << mu_minus << "_xia_" << xi_a << "_xir_" << xi_r
                                         << "_Dphi_" << diffusion_constant << "_kappa_" << kappa << "_rho_" << rho
                                         << "_" << kN << "_" << kM << "_" << kL << ".txt";
  flux_limiter_activity_file_name_ = flux_limiter_activity_file_name_buffer.str();
  if (thread->IsRoot())
  {
    std::remove(flux_limiter_activity_file_name_.c_str());
  }
  flux_limiter_activity_file_.open(flux_limiter_activity_file_name_, std::ios::out | std::ios::app);
  assert(flux_limiter_activity_file_.is_open());
//  }
  MPI_Barrier(MPI_COMM_WORLD);
}

BinaryObserverPtr::~BinaryObserverPtr()
{
  if (thread_->IsRoot())
  {
    if (output_file_.is_open())
    {
      output_file_.close();
    }
    if (summary_statistics_file_.is_open())
    {
      summary_statistics_file_.close();
    }
    if (flux_limiter_activity_file_.is_open())
    {
      flux_limiter_activity_file_.close();
    }
  }
}

void BinaryObserverPtr::SaveSystemState(Real *const system_state, long size, Real t)
{
  // Sequential file output
  thread_->SynchronizeVector(system_state, size);
  if (thread_->IsRoot())
  {
    ValidateSolution(system_state, size, t);
    std::cout << "total mass: "
              << std::accumulate(system_state, system_state + size, (Real) 0.0) * kDx * kDy * kDphi
              << std::endl;

    std::chrono::duration<RealForOutput> elapsed_seconds = std::chrono::system_clock::now() - integration_step_timer_;
    std::cout << "t:" << t << " | integration time:" << elapsed_seconds.count() << "s" << std::endl;
    integration_step_timer_ = std::chrono::system_clock::now();

    if (!(output_time_counter_[0] % output_time_threshold_[0]))
    {
      RealForOutput t_float = RealForOutput(t);
      static std::vector<RealForOutput> system_state_float(size, 0.0);//(system_state, system_state + size);
      std::copy(&system_state[0], &system_state[size], &system_state_float[0]);
      output_file_.write((char *) &t_float, sizeof(RealForOutput));
      output_file_.write((char *) &system_state_float[0], system_state_float.size() * sizeof(RealForOutput));
    }
    ++output_time_counter_[0];
  }

  // Parallel file output
  /*for (int idx : thread_->GetLoopIndices())
  {
    if (!std::isfinite(system_state[idx]))
    {
      std::cout << "isfinite assertion failed!" << std::endl;
      should_terminate_ = 1;
    }
    if (system_state[idx] < 0.0)
    {
      std::cout << "density < 0.0!" << std::endl;
      should_terminate_ = 1;
    }
  } // idx
  int should_terminate_collective = 0;
  MPI_Allreduce(&should_terminate_, &should_terminate_collective, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
  should_terminate_ = should_terminate_collective;
//  MPI_Bcast(&should_terminate_, 1, MPI_INT, 0, MPI_COMM_WORLD);

  Real overall_density = 0.0;
  for (int idx : thread_->GetLoopIndices())
  {
    overall_density += system_state[idx];
  } // idx
  Real overall_denisty_collective = 0.0;
  MPI_Reduce(&overall_density, &overall_denisty_collective, 1, kRealTypeForMpi, MPI_SUM, 0, MPI_COMM_WORLD);
  overall_denisty_collective *= kDx * kDy * kDphi;
  if (thread_->IsRoot())
  {
    std::cout << "total mass: " << overall_denisty_collective << std::endl;

    std::chrono::duration<Real> elapsed_seconds = std::chrono::system_clock::now() - integration_step_timer_;
    std::cout << "t:" << t << " | integration time:" << elapsed_seconds.count() << "s" << std::endl;
    integration_step_timer_ = std::chrono::system_clock::now();
  }

  if (!(output_time_counter_[0] % output_time_threshold_[0]))
  {
    RealForOutput t_float = (RealForOutput) t;
    if (thread_->IsRoot())
    {
      MPI_File output_file;
      MPI_File_open(MPI_COMM_SELF,
                    output_file_name_.c_str(),
                    MPI_MODE_APPEND | MPI_MODE_WRONLY,
                    MPI_INFO_NULL,
                    &output_file);
      MPI_File_write(output_file, &t_float, 1, MPI_FLOAT, MPI_STATUS_IGNORE);
      MPI_File_close(&output_file);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    MPI_File output_file;
    MPI_File_open(MPI_COMM_WORLD,
                  output_file_name_.c_str(),
                  MPI_MODE_APPEND | MPI_MODE_WRONLY,
                  MPI_INFO_NULL,
                  &output_file);
    static std::vector<RealForOutput> system_state_float(thread_->GetNumberOfElementsPerMpichThread(), 0.0);
    std::copy(&system_state[thread_->GetNumberOfElementsPerMpichThread() * thread_->GetRank()],
              &system_state[thread_->GetNumberOfElementsPerMpichThread() * (thread_->GetRank() + 1)],
              system_state_float.begin());
//              std::back_inserter(system_state_float));
    MPI_Offset position;
    MPI_File_get_position(output_file, &position);
    MPI_File_set_view(output_file,
                      position + thread_->GetNumberOfElementsPerMpichThread() * thread_->GetRank() * sizeof(RealForOutput),
                      MPI_FLOAT,
                      MPI_FLOAT,
                      "native",
                      MPI_INFO_NULL);
    MPI_Request request;
    MPI_File_iwrite(output_file,
                    &system_state_float[0],
                    thread_->GetNumberOfElementsPerMpichThread(),
                    MPI_FLOAT,
                    &request);
    MPI_Waitall(1, &request, MPI_STATUS_IGNORE);

    MPI_File_close(&output_file);
  }
  ++output_time_counter_[0];*/
}

void BinaryObserverPtr::ValidateSolution(Real *const system_state, long size, Real t)
{
  for (int idx = 0; idx < size; ++idx)
  {
    if (!std::isfinite(system_state[idx]))
    {
      std::cout << "finiteness assertion failed!" << std::endl;
      should_terminate_ = 1;
      return;
    }

    if (system_state[idx] < 0.0)
    {
      std::cout << "positivity assertion failed: density == " << system_state[idx] << " < 0.0!" << std::endl;
      should_terminate_ = 1;
      return;
    }
  } // idx
}

void BinaryObserverPtr::SaveSummaryStatistics(const Real *const system_state, long size, Real t)
{
  // Sequential computation
  if (thread_->IsRoot())
  {
    if (!(output_time_counter_[1] % output_time_threshold_[1]))
    {
      float t_float = (float) t;
      std::complex<float> polar_order_parameter(0.0f, 0.0f);
      std::complex<float> nematic_order_parameter(0.0f, 0.0f);
      float normalization = 0.0;
      int i = 0, j = 0, k = 0;
      for (int idx = 0; idx < size; ++idx)
      {
        utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
        polar_order_parameter +=
            std::complex<float>(std::cos((float) Phi(k)), std::sin((float) Phi(k))) * float(system_state[idx]);
        nematic_order_parameter += std::complex<float>(std::cos(2 * float(Phi(k))),
                                                       std::sin(2 * float(Phi(k)))) * float(system_state[idx]);
        normalization += system_state[idx];
      } // i
      polar_order_parameter /= normalization;
      nematic_order_parameter /= normalization;

      summary_statistics_file_ << t_float << '\t' << std::abs(polar_order_parameter) << '\t'
                               << std::arg(polar_order_parameter) << '\t'
                               << std::abs(nematic_order_parameter) << '\t'
                               << std::arg(nematic_order_parameter);
      summary_statistics_file_ << std::endl;
    }
    ++output_time_counter_[1];
  }

  // Parallel computation
  /*if (!(output_time_counter_[1] % output_time_threshold_[1]))
  {
    float t_float = (float) t;
    float polar_order_subparameter_x = 0.0f, polar_order_subparameter_y = 0.0f;
    float nematic_order_subparameter_x = 0.0f, nematic_order_subparameter_y = 0.0f;
    float subnormalization = 0.0;
    int i = 0, j = 0, k = 0;
    for (int idx : thread_->GetLoopIndices())
    {
      OneDimIdxToThreeDimIdx(idx, i, j, k);
      polar_order_subparameter_x += (float) std::cos(Phi(k)) * float(system_state[idx]);
      polar_order_subparameter_y += (float) std::sin(Phi(k)) * float(system_state[idx]);
      nematic_order_subparameter_x += (float) std::cos(2.0 * Phi(k)) * float(system_state[idx]);
      nematic_order_subparameter_y += (float) std::sin(2.0 * Phi(k)) * float(system_state[idx]);
      subnormalization += system_state[idx];
    } // idx

    float polar_order_parameter_x = 0.0f, polar_order_parameter_y = 0.0f;
    float nematic_order_parameter_x = 0.0f, nematic_order_parameter_y = 0.0f;
    float normalization = 0.0;
    MPI_Reduce(&polar_order_subparameter_x, &polar_order_parameter_x, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&polar_order_subparameter_y, &polar_order_parameter_y, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&nematic_order_subparameter_x, &nematic_order_parameter_x, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&nematic_order_subparameter_y, &nematic_order_parameter_y, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&subnormalization, &normalization, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (thread_->IsRoot())
    {
      std::complex<float>
          polar_order_parameter(polar_order_parameter_x / normalization, polar_order_parameter_y / normalization);
      std::complex<float>
          nematic_order_parameter(nematic_order_parameter_x / normalization, nematic_order_parameter_y / normalization);
      summary_statistics_file_ << t_float << '\t' << std::abs(polar_order_parameter) << '\t'
                               << std::arg(polar_order_parameter) << '\t'
                               << std::abs(nematic_order_parameter) << '\t'
                               << std::arg(nematic_order_parameter);
      summary_statistics_file_ << std::endl;
    }
  }
  ++output_time_counter_[1];*/
}

// number of applications of flux limiters
// velocities used for CFL conditions
void BinaryObserverPtr::SaveAdditionalInformation(const std::vector<int> &flux_limiter_count,
                                                  Real cfl_a,
                                                  Real cfl_b,
                                                  Real cfl_c,
                                                  Real t)
{
  if (thread_->IsRoot())
  {
    if (!(output_time_counter_[2] % output_time_threshold_[2]))
    {
      flux_limiter_activity_file_ << t << '\t';
      for (int c : flux_limiter_count)
      {
        flux_limiter_activity_file_ << c << '\t';
      } // fl
      flux_limiter_activity_file_ << cfl_a << '\t' << cfl_b << '\t' << cfl_c;
      flux_limiter_activity_file_ << std::endl;
    }
    ++output_time_counter_[2];
  }
}