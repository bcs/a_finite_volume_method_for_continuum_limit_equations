//
// Created by Nikita Kruk on 29.04.20.
//

#include "BinaryObserverPtrForContinuationMethod.hpp"
#include "../ControlEngines/InitialConditions.hpp"

#include <numeric>  // std::accumulate, std::inner_product, std::iota
#include <random>
#include <algorithm> // std::fill, std::adjacent_difference, std::for_each, std::transform
#include <vector>

BinaryObserverPtrForContinuationMethod::BinaryObserverPtrForContinuationMethod(ThreadXySharedMemory *thread,
                                                                               Real *const system_state,
                                                                               long size) :
    BinaryObserverPtr(thread),
    time_counter_for_system_analysis_(0),
    time_counter_threshold_for_system_analysis_(kInverseDt),
    maximum_absolute_spatial_deviations_(),
    spatial_perturbations_converged_(false),
    time_counter_for_perturbation_imposition_(0),
    bifurcation_path_(BifurcationPath::kNone),
    number_of_imposed_smoothings_(0)
{
  if (bifurcation_path_ == BifurcationPath::kFromHomogeneous)
  {
    AddPerturbations(system_state, size);
  }
}

void BinaryObserverPtrForContinuationMethod::ConsiderSpatialDeviations(Real *const system_state, long size, Real t)
{
  thread_->SynchronizeVector(system_state, size);
  if (thread_->IsRoot())
  {
    if (!(time_counter_for_system_analysis_ % time_counter_threshold_for_system_analysis_))
    {
      if (t >= equilibration_time_)
      {
        ComputeMaximumAbsoluteSpatialDeviation(system_state, size, t);
      }
      Real mean_derivative = 0.0, shift = 0.0;
      if (maximum_absolute_spatial_deviations_.size() == averaging_window_)
      {
//        GetMeanDerivative(mean_derivative);
        GetLinearFit(mean_derivative, shift);

        switch (bifurcation_path_)
        {
          case BifurcationPath::kFromHomogeneous:
            if (std::fabs(mean_derivative) < 1e-6)
            {
              should_terminate_ = true;
            }
            break;
          case BifurcationPath::kFromNonhomogeneousWithSmoothing:
          case BifurcationPath::kFromNonhomogeneousWithoutSmoothing:
            if (std::fabs(mean_derivative) < 5e-5)
            {
              should_terminate_ = true;
            }
            break;
          default:;
        }
      }

      if (t >= kT1)
      {
        if (!(time_counter_for_perturbation_imposition_ % frequency_of_perturbation_imposition_))
        {
          if (bifurcation_path_ == BifurcationPath::kFromNonhomogeneousWithSmoothing)
          {
            if (mean_derivative < Real(0))
            {
              SmoothOut(system_state, size);
              ClearTemporalStatistics();
              /*if (number_of_imposed_smoothings_ < 4)
              {
                ++number_of_imposed_smoothings_;
              }*/
            }
          }
        }
        ++time_counter_for_perturbation_imposition_;
      }
    }
    ++time_counter_for_system_analysis_;
  }
  thread_->BroadcastVector(system_state, size);
}

void BinaryObserverPtrForContinuationMethod::ComputeMaximumAbsoluteSpatialDeviation(const Real *const system_state,
                                                                                    long size,
                                                                                    Real t)
{
  static std::vector<Real> spatially_averaged_system_state(kL, Real(0));
  int i(0), j(0), k(0), idx(0);
  for (k = 0; k < kL; ++k)
  {
    spatially_averaged_system_state[k] = 0;
    for (i = 0; i < kN; ++i)
    {
      for (j = 0; j < kM; ++j)
      {
        utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
        spatially_averaged_system_state[k] += system_state[idx];
      } // j
    } // i
    spatially_averaged_system_state[k] /= (kN * kM);
  } // k

  Real max_spatial_deviation(0);
  for (idx = 0; idx < size; ++idx)
  {
    utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
    max_spatial_deviation =
        std::max(max_spatial_deviation, std::fabs(system_state[idx] - spatially_averaged_system_state[k]));
  } // idx

  if (maximum_absolute_spatial_deviations_.size() == averaging_window_)
  {
    maximum_absolute_spatial_deviations_.pop_front();
    time_points_.pop_front();
  }
  maximum_absolute_spatial_deviations_.push_back(max_spatial_deviation);
  time_points_.push_back(t);
}

void BinaryObserverPtrForContinuationMethod::GetMeanDerivative(Real &mean_derivative)
{
  std::list<Real> derivative_of_spatial_deviations(maximum_absolute_spatial_deviations_.size(), Real(0));
  std::adjacent_difference(maximum_absolute_spatial_deviations_.begin(),
                           maximum_absolute_spatial_deviations_.end(),
                           derivative_of_spatial_deviations.begin());
  derivative_of_spatial_deviations.pop_front();
  mean_derivative = std::accumulate(derivative_of_spatial_deviations.begin(),
                                    derivative_of_spatial_deviations.end(),
                                    Real(0)) / derivative_of_spatial_deviations.size();
}

void BinaryObserverPtrForContinuationMethod::GetLinearFit(Real &slope, Real &shift)
{
  assert(time_points_.size() == maximum_absolute_spatial_deviations_.size());
  Real mean_time_point = std::accumulate(time_points_.begin(), time_points_.end(), Real(0)) / time_points_.size();
  Real mean_squared_time_point = std::inner_product(time_points_.begin(),
                                                    time_points_.end(),
                                                    time_points_.begin(),
                                                    Real(0)) / time_points_.size();
  Real mean_deviation =
      std::accumulate(maximum_absolute_spatial_deviations_.begin(), maximum_absolute_spatial_deviations_.end(), Real(0))
          / maximum_absolute_spatial_deviations_.size();
  Real mean_product = std::inner_product(time_points_.begin(),
                                         time_points_.end(),
                                         maximum_absolute_spatial_deviations_.begin(),
                                         Real(0)) / time_points_.size();
  slope =
      (mean_product - mean_time_point * mean_deviation) / (mean_squared_time_point - mean_time_point * mean_time_point);
  shift = mean_deviation - slope * mean_time_point;
}

void BinaryObserverPtrForContinuationMethod::AddPerturbations(Real *const system_state, long size)
{
  auto min_max_iter = std::minmax_element(&system_state[0], &system_state[size]);
  Real variation = 0.01 * (*min_max_iter.second - *min_max_iter.first);
  static std::vector<Real> spatial_perturbations(size, Real(0));
  InitialConditions::QuasiUniformInXY<std::vector<Real>>(spatial_perturbations, size, Real(0), variation, true);
  std::transform(&system_state[0],
                 &system_state[size],
                 spatial_perturbations.begin(),
                 &system_state[0],
                 [](Real x, Real y) { return x + y; });
  // normalize the probability density function
  Real overall_density = std::accumulate(&system_state[0], &system_state[size], Real(0)) * kDx * kDy * kDphi;
  std::for_each(&system_state[0], &system_state[size], [&](Real &ss) { ss /= overall_density; });
}

void BinaryObserverPtrForContinuationMethod::SmoothOut(Real *const system_state, long size) const
{
  static std::vector<Real> spatially_averaged_system_state(kN * kM * kL, Real(0));
  int idx(0), neighbor_idx(0);
  for (int i = 0; i < kN; ++i)
  {
    for (int j = 0; j < kM; ++j)
    {
      for (int k = 0; k < kL; ++k)
      {
        utilities::ThreeDimIdxToOneDimIdx(i, j, k, idx);
        spatially_averaged_system_state[idx] = 0.0;
        const int n_cells = (number_of_imposed_smoothings_ + 1) * std::round(kN * min_smoothing_radius_);
        const int n_neighbors = (2 * n_cells + 1) * (2 * n_cells + 1);
        for (int ii = -n_cells; ii <= n_cells; ++ii)
        {
          for (int jj = -n_cells; jj <= n_cells; ++jj)
          {
            utilities::ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + ii, kN),
                                              utilities::PositiveModulo(j + jj, kM),
                                              k,
                                              neighbor_idx);
            spatially_averaged_system_state[idx] += system_state[neighbor_idx];
          } // jj
        } // ii
        spatially_averaged_system_state[idx] /= n_neighbors;
      } // k
    } // j
  } // i
  std::copy(spatially_averaged_system_state.begin(), spatially_averaged_system_state.end(), &system_state[0]);
  // normalize the probability density function
  Real overall_density = std::accumulate(&system_state[0], &system_state[size], Real(0)) * kDx * kDy * kDphi;
  std::for_each(&system_state[0], &system_state[size], [&](Real &ss) { ss /= overall_density; });
}

void BinaryObserverPtrForContinuationMethod::ClearTemporalStatistics()
{
  maximum_absolute_spatial_deviations_.clear();
  time_points_.clear();
}