//
// Created by Nikita Kruk on 20.01.20.
//

#include "IndexMap.hpp"

IndexMap::IndexMap() :
    three_dim_idx_to_one_dim_idx_(kN, std::vector<std::vector<int>>(kM, std::vector<int>(kL, 0))),
    one_dim_idx_to_three_dim_idx_()
{
  /*int i = 0, j = 0, k = 0;
  for (int idx = 0; idx < kN * kM * kL; ++idx)
  {
    utilities::OneDimIdxToThreeDimIdx(idx, i, j, k);
    three_dim_idx_to_one_dim_idx_[i][j][k] = idx;
    one_dim_idx_to_three_dim_idx_[idx] = {i, j, k};
  } // idx*/
}

IndexMap::~IndexMap()
{
  three_dim_idx_to_one_dim_idx_.clear();
  one_dim_idx_to_three_dim_idx_.clear();
}

void IndexMap::ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx) const
{
  /*idx = three_dim_idx_to_one_dim_idx_[x][y][phi];*/
  utilities::ThreeDimIdxToOneDimIdx(x, y, phi, idx);
}

void IndexMap::OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi) const
{
  /*const std::array<int, kDim> &three_dim_idx = one_dim_idx_to_three_dim_idx_.at(idx);
  x = three_dim_idx[0];
  y = three_dim_idx[1];
  phi = three_dim_idx[2];*/
  utilities::OneDimIdxToThreeDimIdx(idx, x, y, phi);
}