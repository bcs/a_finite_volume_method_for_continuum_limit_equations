//
// Created by Nikita Kruk on 12.06.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_DEFINITIONS_HPP
#define SPC2FVMNONLINNONLOCEQS_DEFINITIONS_HPP

//#define MPI_PARALLELIZATION_IN_XY_TWO_SIDED
//#define OPENMP_MIXED_WITH_MPI_PARALLELIZATION_IN_XY_TWO_SIDED
//#define MPI_PARALLELIZATION_IN_XY_ONE_SIDED
//#define MPI_PARALLELIZATION_IN_XY_ONE_SIDED_DISTRIBUTED
#define MPI_PARALLELIZATION_IN_XY_ONE_SIDED_SHARED_MEMORY
//#define MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED
//#define MPI_PARAMETER_SCAN

//#define LICHTENBERG
//#define BCS_CLUSTER

//#define MULTIPRECISION // works only with nonparallelized functions

#include <cmath>
#include <vector>
//#include <boost/multiprecision/float128.hpp>

typedef double Real; // should be consistent with MPI_DATATYPE
typedef double RealForOutput;

const int kDim = 3; // two spatial coordinates + one directional component
enum class Dimension
{
  kX, kY, kPhi
};

// model-specific constants
const Real kXSize = 1.0;
const Real kYSize = 1.0;
const Real kPhiSize = 2.0 * M_PI;
const Real kXRSize = 1.0 / kXSize;
const Real kYRSize = 1.0 / kYSize;
const Real kPhiRSize = 1.0 / kPhiSize;

const Real kMicroscopicVelocity = 1.0;
const Real kSigma = 1.0;
const Real kRho = 0.1;
const Real kAlpha = 1.2;
//extern Real kAlpha;
const Real kDiffusionConstant = 0.01;
//extern Real kDiffusionConstant;

// discretization-specific constants
const int kBits = 8;
const int kN = 20;
const int kM = 20;
const int kL = (int) std::pow(2.0, kBits);
const int kMc = 1000;

const Real kDx = kXSize / kN;
const Real kDy = kYSize / kM;
const Real kDphi = kPhiSize / kL;
const Real kT0 = 0.0;
const Real kT1 = 200.0;
const Real kDt = 0.01;
const int kInverseDt = 1.0 / kDt;

// implementation-specific constants
const Real kC0 = kDphi / 2.0;
#if defined(MULTIPRECISION)
const Real kC1 = boost::multiprecision::sin(kDphi / 2.0) / (kDphi / 2.0);
const Real kC2 = boost::multiprecision::cos(kDphi / 2.0);
const Real kC3 = boost::multiprecision::sin(kDphi / 2.0);
#else
const Real kC1 = std::sin(kDphi / 2.0) / (kDphi / 2.0);
const Real kC2 = std::cos(kDphi / 2.0);
const Real kC3 = std::sin(kDphi / 2.0);
#endif

// vortex arrays
const Real kVelocity = 1.0 / 10.0;
const Real kMuPlus = 1000.0;
const Real kMuMinus = 100.0;
const Real kXiA = 0.21 / 10.0;
const Real kXiR = 0.11 / 10.0;
const Real kKappa = 10.0;

namespace utilities
{
  int PositiveModulo(int i, int n);
  void ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx);
  void ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx, int num_cells_x, int num_cells_y, int num_cells_phi);
  void OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi);
  void OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi, int num_cells_x, int num_cells_y, int num_cells_phi);
  Real Minmod(const Real &a, const Real &b, const Real &c);
  Real ClassAPeriodicBoundaryDistance(const Real &x_i, const Real &y_i, const Real &x_j, const Real &y_j);
  void ClassAPeriodicBoundaryDistance(const Real &x_i,
                                      const Real &y_i,
                                      const Real &x_j,
                                      const Real &y_j,
                                      Real &dx,
                                      Real &dy);
  Real ClassCPeriodicBoundaryDistance(const Real &x_i, const Real &y_i, const Real &x_j, const Real &y_j);
  void ClassCPeriodicBoundaryDistance(const Real &x_i,
                                      const Real &y_i,
                                      const Real &x_j,
                                      const Real &y_j,
                                      Real &dx,
                                      Real &dy);
  void ApplyPeriodicBoundaryConditions(const Real &x, const Real &y, Real &x_pbc, Real &y_pbc);
  void GenerateGaussLegendrePoints(const Real &lower_limit,
                                   const Real &upper_limit,
                                   int n,
                                   std::vector<Real> &x,
                                   std::vector<Real> &w);
} // namespace utilities
Real X(const Real &i);
Real Y(const Real &j);
Real Phi(const Real &k);

#endif //SPC2FVMNONLINNONLOCEQS_DEFINITIONS_HPP
