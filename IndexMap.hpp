//
// Created by Nikita Kruk on 20.01.20.
//

#ifndef SPC2FINITEVOLUMEMETHODS_INDEXMAP_HPP
#define SPC2FINITEVOLUMEMETHODS_INDEXMAP_HPP

#include "Definitions.hpp"

#include <vector>
#include <unordered_map>
#include <array>

class IndexMap
{
 public:

  IndexMap();
  ~IndexMap();

  void ThreeDimIdxToOneDimIdx(int x, int y, int phi, int &idx) const;
  void OneDimIdxToThreeDimIdx(int idx, int &x, int &y, int &phi) const;

 private:

  std::vector<std::vector<std::vector<int>>> three_dim_idx_to_one_dim_idx_;
  std::unordered_map<int, std::array<int, kDim>> one_dim_idx_to_three_dim_idx_;

};

#endif //SPC2FINITEVOLUMEMETHODS_INDEXMAP_HPP
