//
// Created by Nikita Kruk on 2019-01-03.
//

#ifndef SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGFASTPTR_HPP
#define SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGFASTPTR_HPP

#include "DynSysSplittingPtr.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"

#include <vector>

class DynSysSplittingFastPtr : public DynSysSplittingPtr
{
 public:

  explicit DynSysSplittingFastPtr(ThreadXySharedMemory *thread);
  ~DynSysSplittingFastPtr();

 private:

  std::vector<std::vector<int>> spatial_neighbor_indices_;
  std::vector<Real> cos_kernel_at_spatial_point_;
  std::vector<Real> sin_kernel_at_spatial_point_;

  virtual void CalculateFlux(Dimension dim);
  void CalculateVelocityAtCellInterface(int i,
                                        int j,
                                        std::vector<Real> &velocities);
  void CalculateVelocityPotentialAtCellCenter(int i,
                                              int j,
                                              std::vector<Real> &potentials);

};

#endif //SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGFASTPTR_HPP
