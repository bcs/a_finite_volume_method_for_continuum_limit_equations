//
// Created by Nikita Kruk on 2019-01-03.
//

#include "DynSysSplittingFastPtr.hpp"
#include "../FourierTransforms/Convolution.hpp"

#include <mpi.h>
#include <iostream>
#include <chrono>
#include <numeric> // std::accumulate
#include <algorithm> // std::fill

DynSysSplittingFastPtr::DynSysSplittingFastPtr(ThreadXySharedMemory *thread) :
    DynSysSplittingPtr(thread),
    spatial_neighbor_indices_(kN * kM, std::vector<int>()),
    cos_kernel_at_spatial_point_(kL, 0.0),
    sin_kernel_at_spatial_point_(kL, 0.0)
{
  for (int i = 0; i < kN; ++i)
  {
    for (int j = 0; j < kM; ++j)
    {
      for (int l = 0; l < kN; ++l)
      {
        for (int m = 0; m < kM; ++m)
        {
          if (utilities::ClassCPeriodicBoundaryDistance(X(i), Y(j), X(l), Y(m)) <= kRho)
          {
            spatial_neighbor_indices_[i + kN * j].push_back(l + kN * m);
          }
        } // m
      } // l
//      spatial_neighbor_indices_[i + kN * j].push_back(i + kN * j); // for the hydrodynamic limit only
    } // j
  } // i
  thread_->InitializeNeighborThreads(spatial_neighbor_indices_);

  for (int k = 0; k < kL; ++k)
  {
    cos_kernel_at_spatial_point_[k] = kC1 * std::cos(Phi(k) + kAlpha);
    sin_kernel_at_spatial_point_[k] = -(kC2 - kC1) * std::sin(Phi(k) + kAlpha);
  } // k
}

DynSysSplittingFastPtr::~DynSysSplittingFastPtr()
{

}

void DynSysSplittingFastPtr::CalculateFlux(Dimension dim)
{
  Real density_next_in_current_cell = 0.0, density_prev_in_next_cell = 0.0;
  int i = 0, j = 0;
  int idx = 0, idx_next = 0;
  Real delta = 0.0;
  switch (dim)
  {
    case Dimension::kX: delta = kDx;
      break;
    case Dimension::kY: delta = kDy;
      break;
    case Dimension::kPhi: delta = kDphi;
      break;
  }

  CalculateDensitySlopes(dim);
  VerifyPositivityOfDensityAtCellInterfaces(dim);

  MPI_Win &win_density_slope = thread_->GetWindow(std::string("density_slope_window"));
  MPI_Win &win_rk_system_state = thread_->GetWindow(std::string("rk_system_state_window"));
  MPI_Win &win_flux = thread_->GetWindow(std::string("flux_window"));
  MPI_Win_lock_all(0, win_density_slope);
  MPI_Win_lock_all(0, win_rk_system_state);
  MPI_Win_lock_all(0, win_flux);
  static std::vector<Real> velocities(kL, 0.0);
  for (const int &spatial_index: thread_->GetSpatialLoopIndices())
  {
    i = spatial_index % kN;
    j = spatial_index / kN;

//      std::chrono::time_point<std::chrono::system_clock> timer = std::chrono::system_clock::now();
    if (dim == Dimension::kPhi)
    {
      std::fill(velocities.begin(), velocities.end(), 0.0);
      CalculateVelocityAtCellInterface(i, j, velocities);
    }
//      std::chrono::duration<Real> elapsed_seconds = std::chrono::system_clock::now() - timer;
//      std::cout << "fft convolution: " << elapsed_seconds.count() << "s" << std::endl;
    for (int k = 0; k < kL; ++k)
    {
      index_map_.ThreeDimIdxToOneDimIdx(i, j, k, idx);
      density_next_in_current_cell = rk_system_state_[idx] + delta / 2.0 * density_slope_[idx];
      switch (dim)
      {
        case Dimension::kX: index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, kN), j, k, idx_next);
          break;
        case Dimension::kY: index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j + 1, kM), k, idx_next);
          break;
        case Dimension::kPhi: index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k + 1, kL), idx_next);
          break;
      }
      density_prev_in_next_cell = rk_system_state_[idx_next] - delta / 2.0 * density_slope_[idx_next];

      Real velocity = 0.0;
      switch (dim)
      {
        case Dimension::kX: velocity = kMicroscopicVelocity * 1.0 * cos_of_phase_[k];
          if (cfl_a_ < std::fabs(velocity))
          {
            cfl_a_ = std::fabs(velocity);
          }
          break;
        case Dimension::kY: velocity = kMicroscopicVelocity * 1.0 * sin_of_phase_[k];
          if (cfl_b_ < std::fabs(velocity))
          {
            cfl_b_ = std::fabs(velocity);
          }
          break;
        case Dimension::kPhi: velocity = velocities[k];
          if (cfl_c_ < std::fabs(velocity))
          {
            cfl_c_ = std::fabs(velocity);
          }
          break;
      }
      flux_[idx] = std::max((Real) 0.0, velocity) * density_next_in_current_cell
          + std::min((Real) 0.0, velocity) * density_prev_in_next_cell;
    } // k
  } // spatial_index
//  MPI_Win_sync(win_density_slope);
//  MPI_Win_sync(win_rk_system_state);
  MPI_Win_sync(win_flux);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win_density_slope);
  MPI_Win_unlock_all(win_rk_system_state);
  MPI_Win_unlock_all(win_flux);
  thread_->SynchronizeVectorThroughoutClusters(flux_);
}

/**
 * For the terms with convolution only
 * @param i
 * @param j
 * @param velocities
 */
void DynSysSplittingFastPtr::CalculateVelocityAtCellInterface(int i,
                                                              int j,
                                                              std::vector<Real> &velocities)
{
  static std::vector<Real> potentials(kL, 0.0);
  std::fill(potentials.begin(), potentials.end(), Real(0.0));
  CalculateVelocityPotentialAtCellCenter(i, j, potentials);
  for (int k = 0; k < kL; ++k)
  {
    velocities[k] = -(potentials[utilities::PositiveModulo(k + 1, kL)] - potentials[k]) / kDphi;
  } // k
}

/**
 * Calculation of a potential through convolution only
 * @param i
 * @param j
 * @param potentials
 */
void DynSysSplittingFastPtr::CalculateVelocityPotentialAtCellCenter(int i,
                                                                    int j,
                                                                    std::vector<Real> &potentials)
{
// \sum_{l,m}
  int l = 0, m = 0;
  static std::vector<Real> projected_system_state(kL, 0.0), projected_density_slope(kL, 0.0);
  std::fill(projected_system_state.begin(), projected_system_state.end(), 0.0);
  std::fill(projected_density_slope.begin(), projected_density_slope.end(), 0.0);

  const std::vector<int> &spatial_neighbor_indices = spatial_neighbor_indices_[i + kN * j];
  for (const int &spatial_neighbor_index : spatial_neighbor_indices)
  {
    l = spatial_neighbor_index % kN;
    m = spatial_neighbor_index / kN;
    for (int n = 0; n < kL; ++n)
    {
      int idx = 0;
      index_map_.ThreeDimIdxToOneDimIdx(l, m, n, idx);
      projected_system_state[n] += rk_system_state_[idx];
      projected_density_slope[n] += density_slope_[idx];
    } // n
  } // spatial_neighbor_index

  // convolution of the interaction term
  Convolution convolution;
  static std::vector<Real> convolution_at_spatial_point_0(kL, 0.0);
  convolution.FastConvolution(projected_system_state, cos_kernel_at_spatial_point_, convolution_at_spatial_point_0);
  static std::vector<Real> convolution_at_spatial_point_1(kL, 0.0);
  convolution.FastConvolution(projected_density_slope, sin_kernel_at_spatial_point_, convolution_at_spatial_point_1);

  for (int k = 0; k < kL; ++k)
  {
    potentials[k] = convolution_at_spatial_point_0[k] + convolution_at_spatial_point_1[k];
  } // k
  Real normalization = std::accumulate(projected_system_state.begin(), projected_system_state.end(), Real(0.0));

  // scaling of interaction force and diffusion
  for (int k = 0; k < kL; ++k)
  {
    potentials[k] *= (-kSigma / normalization);
    if (kDiffusionConstant > Real(0))
    {
      int idx = 0;
      index_map_.ThreeDimIdxToOneDimIdx(i, j, k, idx);
      potentials[k] += (kDiffusionConstant * std::log(rk_system_state_[idx]));
    }
  } // k
}