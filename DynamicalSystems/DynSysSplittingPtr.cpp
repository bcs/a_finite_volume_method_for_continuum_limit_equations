//
// Created by Nikita Kruk on 2019-01-03.
//

#include "DynSysSplittingPtr.hpp"

#include <mpi.h>
#include <iostream>
#include <algorithm> // std::min
#include <cassert>

DynSysSplittingPtr::DynSysSplittingPtr(ThreadXySharedMemory *thread) :
    thread_(thread),
    index_map_(),
    sin_of_phase_(kL, 0.0),
    cos_of_phase_(kL, 0.0),
    vector_size_(kN * kM * kL),
//    flux_((unsigned long) (kN * kM * kL), 0.0),
    flux_limiter_count_(kDim, 0)
{
  for (int k = 0; k < kL; ++k)
  {
    sin_of_phase_[k] = std::sin(Phi(k));
    cos_of_phase_[k] = std::cos(Phi(k));
  } // k

  if (thread_->IsSharedRoot())
  {
    thread_->AllocateSharedWindow(vector_size_, rk_system_state_, std::string("rk_system_state_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
  } else
  {
    thread->AllocateSharedWindow(0, rk_system_state_, std::string("rk_system_state_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
    MPI_Aint size;
    int disp_unit;
    MPI_Win_shared_query(thread_->GetWindow(std::string("rk_system_state_window")),
                         thread_->GetSharedRootRank(),
                         &size,
                         &disp_unit,
                         &rk_system_state_);
    assert(vector_size_ == (size / sizeof(Real)));
  }
//  MPI_Barrier(thread_->GetSharedCommunicator());

  if (thread_->IsSharedRoot())
  {
    thread_->AllocateSharedWindow(vector_size_, density_slope_, std::string("density_slope_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
  } else
  {
    thread->AllocateSharedWindow(0, density_slope_, std::string("density_slope_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
    MPI_Aint size;
    int disp_unit;
    MPI_Win_shared_query(thread_->GetWindow(std::string("density_slope_window")),
                         thread_->GetSharedRootRank(),
                         &size,
                         &disp_unit,
                         &density_slope_);
    assert(vector_size_ == (size / sizeof(Real)));
  }
//  MPI_Barrier(thread_->GetSharedCommunicator());

  if (thread_->IsSharedRoot())
  {
    thread_->AllocateSharedWindow(vector_size_, flux_, std::string("flux_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
  } else
  {
    thread->AllocateSharedWindow(0, flux_, std::string("flux_window"));
    MPI_Barrier(thread_->GetSharedCommunicator());
    MPI_Aint size;
    int disp_unit;
    MPI_Win_shared_query(thread_->GetWindow(std::string("flux_window")),
                         thread_->GetSharedRootRank(),
                         &size,
                         &disp_unit,
                         &flux_);
    assert(vector_size_ == (size / sizeof(Real)));
  }
//  MPI_Barrier(thread_->GetSharedCommunicator());
}

DynSysSplittingPtr::~DynSysSplittingPtr()
{
  sin_of_phase_.clear();
  cos_of_phase_.clear();

  MPI_Barrier(thread_->GetSharedCommunicator());
  thread_->FreeSharedWindow(std::string("rk_system_state_window"));
  thread_->FreeSharedWindow(std::string("density_slope_window"));
  thread_->FreeSharedWindow(std::string("flux_window"));
}

/**
 * This version is designed to work with a Runge-Kutta stepper
 * @param system_state
 * @param k_prev
 * @param k_next
 * @param k_coef
 * @param dt
 */
Real DynSysSplittingPtr::CalculateAdvection(const Real *const system_state,
                                            const std::vector<Real> &k_prev,
                                            std::vector<Real> &k_next,
                                            Real k_coef,
                                            Real dt)
{
  cfl_a_ = 0.0;
  cfl_b_ = 0.0;

  /*MPI_Win &win_sys = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win_sys);*/
  MPI_Win &win_rk_sys = thread_->GetWindow(std::string("rk_system_state_window"));
  MPI_Win_lock_all(0, win_rk_sys);
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    rk_system_state_[idx] = system_state[idx] + k_coef * dt * k_prev[idx];
  } // idx
  MPI_Win_sync(win_rk_sys);
  MPI_Barrier(thread_->GetSharedCommunicator());
  /*MPI_Win_unlock_all(win_sys);*/
  MPI_Win_unlock_all(win_rk_sys);
  thread_->SynchronizeVectorThroughoutClusters(rk_system_state_);

//  std::fill(flux_.begin(), flux_.end(), 0.0);
  CalculateFlux(Dimension::kX);
//  thread_->SynchronizeVector(Dimension::kX, flux_);

  int i = 0, j = 0, k = 0;
  int idx_prev_x = 0;
  MPI_Win &win_flux = thread_->GetWindow(std::string("flux_window"));
  MPI_Win_lock_all(0, win_flux);
  for (int idx : loop_indices)
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev_x);
    k_next[idx] = -(flux_[idx] - flux_[idx_prev_x]) / kDx;
  } // idx
  MPI_Win_unlock_all(win_flux);

//  std::fill(flux_.begin(), flux_.end(), 0.0);
  CalculateFlux(Dimension::kY);
//  thread_->SynchronizeVector(Dimension::kY, flux_);

  int idx_prev_y = 0;
  MPI_Win_lock_all(0, win_flux);
  for (int idx : loop_indices)
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev_y);
    k_next[idx] += -(flux_[idx] - flux_[idx_prev_y]) / kDy;
  } // idx
  MPI_Win_unlock_all(win_flux);

  /*if (thread_->IsRoot())
  {
    std::cout << "CFL: " << dt << " <= " << std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_)) << std::endl;
  }*/
  return std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_));
}

/**
 * This version is designed to work with a Runge-Kutta stepper
 * @param system_state
 * @param k_prev
 * @param k_next
 * @param k_coef
 * @param dt
 */
Real DynSysSplittingPtr::CalculateSynchronization(const Real *const system_state,
                                                  const std::vector<Real> &k_prev,
                                                  std::vector<Real> &k_next,
                                                  Real k_coef,
                                                  Real dt)
{
  cfl_c_ = 0.0;

  /*MPI_Win &win_sys = thread_->GetWindow(std::string("system_state_window"));
  MPI_Win_lock_all(0, win_sys);*/
  MPI_Win &win_rk_sys = thread_->GetWindow(std::string("rk_system_state_window"));
  MPI_Win_lock_all(0, win_rk_sys);
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    // Note system_state is intentionally kept unsynchronized
    rk_system_state_[idx] = system_state[idx] + k_coef * dt * k_prev[idx];
  } // idx
  MPI_Win_sync(win_rk_sys);
  MPI_Barrier(thread_->GetSharedCommunicator());
  /*MPI_Win_unlock_all(win_sys);*/
  MPI_Win_unlock_all(win_rk_sys);
  thread_->SynchronizeVectorThroughoutClusters(rk_system_state_);

//  std::fill(flux_.begin(), flux_.end(), 0.0);
  CalculateFlux(Dimension::kPhi);
//  // no need to synchronize flux in \varphi direction using this type of paralellization

  int i = 0, j = 0, k = 0;
  int idx_prev_phi = 0;
  MPI_Win &win_flux = thread_->GetWindow(std::string("flux_window"));
  MPI_Win_lock_all(0, win_flux);
  for (int idx : loop_indices)
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev_phi);
    k_next[idx] = -(flux_[idx] - flux_[idx_prev_phi]) / kDphi;
  } // idx
  MPI_Win_unlock_all(win_flux);

  /*if (thread_->IsRoot())
  {
    std::cout << "CFL: " << dt << " <= " << kDphi / (2.0 * cfl_c_) << std::endl;
  }*/
  return kDphi / (2.0 * cfl_c_);
}

void DynSysSplittingPtr::CalculateFlux(Dimension dim)
{

}

/**
 *
 * @param dim : 0, 1, 2
 * @param system_state
 */
void DynSysSplittingPtr::CalculateDensitySlopes(Dimension dim)
{
  int i = 0, j = 0, k = 0;
  int idx_next = 0, idx_prev = 0;
  Real delta = 0.0;
  MPI_Win &win_density_slope = thread_->GetWindow(std::string("density_slope_window"));
  MPI_Win &win_rk_system_state = thread_->GetWindow(std::string("rk_system_state_window"));
  MPI_Win_lock_all(0, win_density_slope);
  MPI_Win_lock_all(0, win_rk_system_state);
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    switch (dim)
    {
      case Dimension::kX : index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, kN), j, k, idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev);
        delta = kDx;
        break;
      case Dimension::kY : index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j + 1, kM), k, idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev);
        delta = kDy;
        break;
      case Dimension::kPhi : index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k + 1, kL), idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev);
        delta = kDphi;
        break;
    }
    density_slope_[idx] = (rk_system_state_[idx_next] - rk_system_state_[idx_prev]) / (2.0 * delta);
  } // idx
  MPI_Win_sync(win_density_slope);
//  MPI_Win_sync(win_rk_system_state);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win_density_slope);
  MPI_Win_unlock_all(win_rk_system_state);
  thread_->SynchronizeVectorThroughoutClusters(density_slope_);
}

/**
 *
 * @param dim : 0, 1, 2
 * @param system_state
 */
void DynSysSplittingPtr::VerifyPositivityOfDensityAtCellInterfaces(Dimension dim)
{
  Real density_next = 0.0, density_prev = 0.0;
  Real delta = 0.0;
  switch (dim)
  {
    case Dimension::kX : delta = kDx;
      break;
    case Dimension::kY : delta = kDy;
      break;
    case Dimension::kPhi : delta = kDphi;
      break;
  }
  MPI_Win &win_density_slope = thread_->GetWindow(std::string("density_slope_window"));
  MPI_Win &win_rk_system_state = thread_->GetWindow(std::string("rk_system_state_window"));
  MPI_Win_lock_all(0, win_density_slope);
  MPI_Win_lock_all(0, win_rk_system_state);
  const std::vector<int> &loop_indices = thread_->GetLoopIndices();
  for (int idx : loop_indices)
  {
    density_next = rk_system_state_[idx] + delta / 2.0 * density_slope_[idx];
    density_prev = rk_system_state_[idx] - delta / 2.0 * density_slope_[idx];
    if (density_prev < 0.0)
    {
      RecalculateDensitySlopeWithFluxLimiter(dim, idx, true);
    } else if (density_next < 0.0)
    {
      RecalculateDensitySlopeWithFluxLimiter(dim, idx, false);
    }
  } // idx
  MPI_Win_sync(win_density_slope);
//  MPI_Win_sync(win_rk_system_state);
  MPI_Barrier(thread_->GetSharedCommunicator());
  MPI_Win_unlock_all(win_density_slope);
  MPI_Win_unlock_all(win_rk_system_state);
  thread_->SynchronizeVectorThroughoutClusters(density_slope_);
}

void DynSysSplittingPtr::RecalculateDensitySlopeWithFluxLimiter(Dimension dim,
                                                                int idx,
                                                                bool left_is_negative)
{
  int i = 0, j = 0, k = 0;
  int idx_next = 0, idx_prev = 0;
  Real delta = 0.0;
  index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);

  switch (dim)
  {
    case Dimension::kX : index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, kN), j, k, idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev);
      delta = kDx;
      break;
    case Dimension::kY : index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j + 1, kM), k, idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev);
      delta = kDy;
      break;
    case Dimension::kPhi : index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k + 1, kL), idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev);
      delta = kDphi;
      break;
  }

  Real theta = 1.0;
  density_slope_[idx] = utilities::Minmod(theta * (rk_system_state_[idx_next] - rk_system_state_[idx]) / delta,
                                          density_slope_[idx],
                                          theta * (rk_system_state_[idx] - rk_system_state_[idx_prev]) / delta);
  /*// max possible density slope
  if (left_is_negative)
  {
    density_slope_[idx] = rk_system_state_[idx] / (delta / 2.0);
  } else
  {
    density_slope_[idx] = -rk_system_state_[idx] / (delta / 2.0);
  }*/

  switch (dim)
  {
    case Dimension::kX : ++flux_limiter_count_[0];
      break;
    case Dimension::kY : ++flux_limiter_count_[1];
      break;
    case Dimension::kPhi : ++flux_limiter_count_[2];
      break;
  }
}

void DynSysSplittingPtr::ResetFluxLimiterCount()
{
  std::fill(flux_limiter_count_.begin(), flux_limiter_count_.end(), 0);
}