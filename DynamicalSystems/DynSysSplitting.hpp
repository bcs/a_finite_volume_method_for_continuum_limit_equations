//
// Created by Nikita Kruk on 05.11.18.
//

#ifndef SPC2FVMNONLINNONLOCEQS_DYNAMICALSYSTEMSTRANGSPLITTING_HPP
#define SPC2FVMNONLINNONLOCEQS_DYNAMICALSYSTEMSTRANGSPLITTING_HPP

#include "../Definitions.hpp"
#include "../Parallelization/Thread.hpp"
#include "../IndexMap.hpp"

#include <vector>
#include <array>

/**
 * Dynamical system computes the RHS of a given PDE
 * using the Strang splitting for
 * advection (\Delta t / 2), synchronization (\Delta t), and advection (\Delta t / 2) steps
 */
class DynSysSplitting
{
 public:

  explicit DynSysSplitting(Thread *thread);
  virtual ~DynSysSplitting();

  Real CalculateAdvection(const std::vector<Real> &system_state,
                          std::vector<Real> &flux_x,
                          std::vector<Real> &flux_y,
                          Real dt);
  Real CalculateAdvection(const std::vector<Real> &system_state,
                          const std::vector<Real> &k_prev,
                          std::vector<Real> &k_next,
                          Real k_coef,
                          Real dt);
  Real CalculateSynchronization(const std::vector<Real> &system_state,
                                std::vector<Real> &flux_phi,
                                Real dt);
  Real CalculateSynchronization(const std::vector<Real> &system_state,
                                const std::vector<Real> &k_prev,
                                std::vector<Real> &k_next,
                                Real k_coef,
                                Real dt);

  void ResetFluxLimiterCount();
  const std::vector<int> &GetFluxLimiterCount() const
  { return flux_limiter_count_; }
  Real GetCflA() const
  { return cfl_a_; }
  Real GetCflB() const
  { return cfl_b_; }
  Real GetCflC() const
  { return cfl_c_; }

 protected:

  Thread *thread_;
  IndexMap index_map_;
  std::vector<Real> sin_of_phase_;
  std::vector<Real> cos_of_phase_;
  std::vector<Real> density_slope_;
  Real cfl_a_;
  Real cfl_b_;
  Real cfl_c_;
  std::vector<int> flux_limiter_count_;

  virtual void CalculateFlux(Dimension dim, const std::vector<Real> &system_state, std::vector<Real> &flux) = 0;
  void CalculateDensitySlopes(Dimension dim, const std::vector<Real> &system_state);
  void VerifyPositivityOfDensityAtCellInterfaces(Dimension dim, const std::vector<Real> &system_state);
  void RecalculateDensitySlopeWithFluxLimiter(Dimension dim,
                                              const std::vector<Real> &system_state,
                                              int idx,
                                              bool left_is_negative);

 private:

};

#endif //SPC2FVMNONLINNONLOCEQS_DYNAMICALSYSTEMSTRANGSPLITTING_HPP
