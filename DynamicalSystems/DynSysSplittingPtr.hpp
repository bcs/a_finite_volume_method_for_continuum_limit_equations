//
// Created by Nikita Kruk on 2019-01-03.
//

#ifndef SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGPTR_HPP
#define SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGPTR_HPP

#include "../Definitions.hpp"
#include "../Parallelization/ThreadXySharedMemory.hpp"
#include "../IndexMap.hpp"

#include <vector>

/**
 * Dynamical system computes the RHS of a given PDE
 * using the Strang splitting for
 * advection (\Delta t / 2), synchronization (\Delta t), and advection (\Delta t / 2) steps
 */
class DynSysSplittingPtr
{
 public:

  explicit DynSysSplittingPtr(ThreadXySharedMemory *thread);
  virtual ~DynSysSplittingPtr();

  Real CalculateAdvection(const Real *const system_state,
                          const std::vector<Real> &k_prev,
                          std::vector<Real> &k_next,
                          Real k_coef,
                          Real dt);
  Real CalculateSynchronization(const Real *const system_state,
                                const std::vector<Real> &k_prev,
                                std::vector<Real> &k_next,
                                Real k_coef,
                                Real dt);

  void ResetFluxLimiterCount();
  const std::vector<int> &GetFluxLimiterCount() const
  { return flux_limiter_count_; }
  Real GetCflA() const
  { return cfl_a_; }
  Real GetCflB() const
  { return cfl_b_; }
  Real GetCflC() const
  { return cfl_c_; }

  ThreadXySharedMemory *thread_;
  IndexMap index_map_;
  std::vector<Real> sin_of_phase_;
  std::vector<Real> cos_of_phase_;
  Real *rk_system_state_;
  Real *density_slope_;
  Real *flux_;
//  std::vector<Real> flux_;
  long vector_size_;
  Real cfl_a_;
  Real cfl_b_;
  Real cfl_c_;
  std::vector<int> flux_limiter_count_;

  virtual void CalculateFlux(Dimension dim) = 0;
  void CalculateDensitySlopes(Dimension dim);
  void VerifyPositivityOfDensityAtCellInterfaces(Dimension dim);
  void RecalculateDensitySlopeWithFluxLimiter(Dimension dim,
                                              int idx,
                                              bool left_is_negative);

 private:

};

#endif //SPC2FINITEVOLUMEMETHODS_DYNSYSSPLITTINGPTR_HPP
