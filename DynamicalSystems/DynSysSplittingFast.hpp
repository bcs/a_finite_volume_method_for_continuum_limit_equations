//
// Created by Nikita Kruk on 2018-12-11.
//

#ifndef SPC2FINITEVOLUMEMETHODS_DYNAMICALSYSTEMSTRANGSPLITTINGFAST_HPP
#define SPC2FINITEVOLUMEMETHODS_DYNAMICALSYSTEMSTRANGSPLITTINGFAST_HPP

#include "DynSysSplitting.hpp"
#include "../Parallelization/Thread.hpp"

#include <vector>

class DynSysSplittingFast : public DynSysSplitting
{
 public:

  explicit DynSysSplittingFast(Thread *thread);
  ~DynSysSplittingFast();

 private:

  std::vector<std::vector<int>> spatial_neighbor_indices_;
  std::vector<Real> cos_kernel_at_spatial_point_;
  std::vector<Real> sin_kernel_at_spatial_point_;

  virtual void CalculateFlux(Dimension dim, const std::vector<Real> &system_state, std::vector<Real> &flux);
  void CalculateVelocityAtCellInterface(const std::vector<Real> &system_state,
                                        int i,
                                        int j,
                                        std::vector<Real> &velocities);
  void CalculateVelocityPotentialAtCellCenter(const std::vector<Real> &system_state,
                                              int i,
                                              int j,
                                              std::vector<Real> &potentials);

};

#endif //SPC2FINITEVOLUMEMETHODS_DYNAMICALSYSTEMSTRANGSPLITTINGFAST_HPP
