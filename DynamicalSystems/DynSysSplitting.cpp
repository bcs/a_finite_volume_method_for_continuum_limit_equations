//
// Created by Nikita Kruk on 05.11.18.
//

#include "DynSysSplitting.hpp"

#include <iostream>
#include <algorithm> // std::min

DynSysSplitting::DynSysSplitting(Thread *thread) :
    thread_(thread),
    sin_of_phase_(kL, 0.0),
    cos_of_phase_(kL, 0.0),
    density_slope_(kN * kM * kL, 0.0),
    flux_limiter_count_(kDim, 0)
{
#if defined(MULTIPRECISION)
  using boost::multiprecision::sin, boost::multiprecision::cos;
#else
  using std::sin, std::cos;
#endif

  for (int k = 0; k < kL; ++k)
  {
    sin_of_phase_[k] = sin(Phi(k));
    cos_of_phase_[k] = cos(Phi(k));
  } // k
}

DynSysSplitting::~DynSysSplitting()
{
  sin_of_phase_.clear();
  cos_of_phase_.clear();
  density_slope_.clear();
}

Real DynSysSplitting::CalculateAdvection(const std::vector<Real> &system_state,
                                         std::vector<Real> &flux_x,
                                         std::vector<Real> &flux_y,
                                         Real dt)
{
  cfl_a_ = 0.0;
  cfl_b_ = 0.0;

  CalculateFlux(Dimension::kX, system_state, flux_x);
  CalculateFlux(Dimension::kY, system_state, flux_y);

  /*if (thread_.IsRoot())
  {
    std::cout << "CFL: " << dt << " <= " << std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_)) << std::endl;
  }*/
  return std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_));
}

/**
 * This version is designed to work with a Runge-Kutta-4 stepper
 * @param system_state
 * @param k_prev
 * @param k_next
 * @param k_coef
 * @param dt
 */
Real DynSysSplitting::CalculateAdvection(const std::vector<Real> &system_state,
                                         const std::vector<Real> &k_prev,
                                         std::vector<Real> &k_next,
                                         Real k_coef,
                                         Real dt)
{
  cfl_a_ = 0.0;
  cfl_b_ = 0.0;

  std::vector<Real> rk_system_state(system_state.size(), 0.0);
//  for (int idx : thread_->GetLoopIndices())
  for (int idx = 0; idx < system_state.size(); ++idx)
  {
    rk_system_state[idx] = system_state[idx] + k_coef * dt * k_prev[idx];
  } // idx
//  thread_->SynchronizeVector(rk_system_state);

  std::vector<Real> flux_x(system_state.size(), (Real) 0.0);
  CalculateFlux(Dimension::kX, rk_system_state, flux_x);
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
#else
  thread_->SynchronizeVector(flux_x);
#endif
  int i = 0, j = 0, k = 0;
  int idx_prev_x = 0;
  for (int idx : thread_->GetLoopIndices())
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev_x);
    k_next[idx] = -(flux_x[idx] - flux_x[idx_prev_x]) / kDx;
  } // idx

  // reuse flux_x container so as not to allocate additional space
  std::vector<Real> &flux_y = flux_x;
  CalculateFlux(Dimension::kY, rk_system_state, flux_y);
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
#else
  thread_->SynchronizeVector(flux_y);
#endif
  int idx_prev_y = 0;
  for (int idx : thread_->GetLoopIndices())
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev_y);
    k_next[idx] += -(flux_y[idx] - flux_y[idx_prev_y]) / kDy;
  } // idx

  /*if (thread_->IsRoot())
  {
    std::cout << "CFL: " << dt << " <= " << std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_)) << std::endl;
  }*/
  return std::min(kDx / (4.0 * cfl_a_), kDy / (4.0 * cfl_b_));
}

Real DynSysSplitting::CalculateSynchronization(const std::vector<Real> &system_state,
                                               std::vector<Real> &flux_phi,
                                               Real dt)
{
  cfl_c_ = 0.0;

#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
  CalculateFlux(Dimension::kPhi, system_state, flux_phi);
#else
  CalculateFlux(Dimension::kPhi, system_state, flux_phi);
#endif

  /*if (thread_->IsRoot())
  {
    std::cout << "CFL: " << dt << " <= " << kDphi / (2.0 * cfl_c_) << std::endl;
  }*/
  return kDphi / (2.0 * cfl_c_);
}

/**
 * This version is designed to work with a Runge-Kutta-4 stepper
 * @param system_state
 * @param k_prev
 * @param k_next
 * @param k_coef
 * @param dt
 */
Real DynSysSplitting::CalculateSynchronization(const std::vector<Real> &system_state,
                                               const std::vector<Real> &k_prev,
                                               std::vector<Real> &k_next,
                                               Real k_coef,
                                               Real dt)
{
  cfl_c_ = 0.0;

  std::vector<Real> rk_system_state(system_state.size(), 0.0);
  // TODO: parallelization?
//  for (int idx : thread_->GetLoopIndices())
  for (int idx = 0; idx < system_state.size(); ++idx)
  {
    rk_system_state[idx] = system_state[idx] + k_coef * dt * k_prev[idx];
  } // idx
//  thread_->SynchronizeVector(rk_system_state);

  std::vector<Real> flux_phi(system_state.size(), (Real) 0.0);
#if defined(MPI_PARALLELIZATIOIN_IN_PHI_TWO_SIDED)
  CalculateFlux(Dimension::kPhi, rk_system_state, flux_phi);
  thread_.SynchronizeVector(flux_phi);
#else
  CalculateFlux(Dimension::kPhi, rk_system_state, flux_phi);
  // No need to synchronize flux_phi, if the whole phi-range is in one thread
//  thread_->SynchronizeVector(flux_phi);
#endif

  int i = 0, j = 0, k = 0;
  int idx_prev_phi = 0;
  for (int idx : thread_->GetLoopIndices())
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev_phi);
    k_next[idx] = -(flux_phi[idx] - flux_phi[idx_prev_phi]) / kDphi;
  } // idx

//  if (thread_.IsRoot())
//  {
//    std::cout << "CFL: " << dt << " <= " << kDphi / (2.0 * cfl_c_) << std::endl;
//  }
  return kDphi / (2.0 * cfl_c_);
}

void DynSysSplitting::CalculateFlux(Dimension dim,
                                    const std::vector<Real> &system_state,
                                    std::vector<Real> &flux)
{

}

/**
 *
 * @param dim : 0, 1, 2
 * @param system_state
 */
void DynSysSplitting::CalculateDensitySlopes(Dimension dim, const std::vector<Real> &system_state)
{
  int i = 0, j = 0, k = 0;
  int idx_next = 0, idx_prev = 0;
  Real delta = 0.0;
  for (int idx : thread_->GetLoopIndices())
  {
    index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);
    switch (dim)
    {
      case Dimension::kX : index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, kN), j, k, idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev);
        delta = kDx;
        break;
      case Dimension::kY : index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j + 1, kM), k, idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev);
        delta = kDy;
        break;
      case Dimension::kPhi : index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k + 1, kL), idx_next);
        index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev);
        delta = kDphi;
        break;
    }
    density_slope_[idx] = (system_state[idx_next] - system_state[idx_prev]) / (2.0 * delta);
  } // idx
}

/**
 *
 * @param dim : 0, 1, 2
 * @param system_state
 */
void DynSysSplitting::VerifyPositivityOfDensityAtCellInterfaces(Dimension dim,
                                                                const std::vector<Real> &system_state)
{
  Real density_next = 0.0, density_prev = 0.0;
  Real delta = 0.0;
  switch (dim)
  {
    case Dimension::kX : delta = kDx;
      break;
    case Dimension::kY : delta = kDy;
      break;
    case Dimension::kPhi : delta = kDphi;
      break;
  }
  for (int idx : thread_->GetLoopIndices())
  {
    density_next = system_state[idx] + delta / 2.0 * density_slope_[idx];
    density_prev = system_state[idx] - delta / 2.0 * density_slope_[idx];
    if (density_prev < 0.0)
    {
      RecalculateDensitySlopeWithFluxLimiter(dim, system_state, idx, true);
    } else if (density_next < 0.0)
    {
      RecalculateDensitySlopeWithFluxLimiter(dim, system_state, idx, false);
    }
  } // idx
}

void DynSysSplitting::RecalculateDensitySlopeWithFluxLimiter(Dimension dim,
                                                             const std::vector<Real> &system_state,
                                                             int idx,
                                                             bool left_is_negative)
{
  int i = 0, j = 0, k = 0;
  int idx_next = 0, idx_prev = 0;
  Real delta = 0.0;
  index_map_.OneDimIdxToThreeDimIdx(idx, i, j, k);

  switch (dim)
  {
    case Dimension::kX : index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i + 1, kN), j, k, idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(utilities::PositiveModulo(i - 1, kN), j, k, idx_prev);
      delta = kDx;
      break;
    case Dimension::kY : index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j + 1, kM), k, idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(i, utilities::PositiveModulo(j - 1, kM), k, idx_prev);
      delta = kDy;
      break;
    case Dimension::kPhi : index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k + 1, kL), idx_next);
      index_map_.ThreeDimIdxToOneDimIdx(i, j, utilities::PositiveModulo(k - 1, kL), idx_prev);
      delta = kDphi;
      break;
  }

  Real theta = 1.0;
  density_slope_[idx] = utilities::Minmod(theta * (system_state[idx_next] - system_state[idx]) / delta,
                               density_slope_[idx],
                               theta * (system_state[idx] - system_state[idx_prev]) / delta);
  /*// max possible density slope
  if (left_is_negative)
  {
    density_slope_[idx] = system_state[idx] / (delta / 2.0);
  } else
  {
    density_slope_[idx] = -system_state[idx] / (delta / 2.0);
  }*/

  switch (dim)
  {
    case Dimension::kX : ++flux_limiter_count_[0];
      break;
    case Dimension::kY : ++flux_limiter_count_[1];
      break;
    case Dimension::kPhi : ++flux_limiter_count_[2];
      break;
  }
}

void DynSysSplitting::ResetFluxLimiterCount()
{
  std::fill(flux_limiter_count_.begin(), flux_limiter_count_.end(), 0);
}